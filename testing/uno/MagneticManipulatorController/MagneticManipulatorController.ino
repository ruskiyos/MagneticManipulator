const int PIN_COIL = 11; // Pins 3 and 11 are connected to Timer 2.
const int PIN_HALL_SENSOR = 1;

//current mode indicator lights red, yellow, and green respectively
const int PIN_STATE_MODE_OFF = 4;
const int PIN_STATE_MODE_IDLE = 5;
const int PIN_STATE_MODE_PID_CONTROL = 6;

//time between bewteen exectutions of a check and update of the current system state ms 
const int CONTROL_UPDATE_FREQ = 1;

const int AUTO_PRINT_FREQ = 2000;

// Minimum magnetic field when in idle mode (needed to properly orientate the magnet).
const int IDLE_PWM = 50;
const int MAX_PWM = 255; 
const int INIT_PWM_MIDPOINT = 37;
const int INIT_SET_POINT = 560; 

//system will change state from IDLE to PID_CONTROL when the magnet enters this interval
const int INIT_CONTROL_INTERVAL_MAX = 540;
const int INIT_CONTROL_INTERVAL_MIN = 580;
//The range that the system will countinue to operate in control mode for 
const int MIN_CONTROL_DISTANCE = 520;
const int MAX_CONTROL_DISTANCE = 1000;

//the maximume cumilative error the sytem should allow in its calculations
const int MAX_I = 1000;

// State machine constants.
const byte MODE_OFF = 0; 
const byte MODE_IDLE = 1; 
const byte MODE_PID_CONTROL = 2;

//PID control wieghts 
const float INIT_KP = 2.1;
const float INIT_KI = 0.001;
const float INIT_KD = 23.6;

//PID control wieght adjustment amounts used for tuning PID control algorythm
const float ADJ_KP = .1;
const float ADJ_KI = .001;
const float ADJ_KD = .1;

int currMode;
int currPWM;
int currPWMMidpoint;
int currSetPoint;
int currHallVal;
int prevHallVal;
unsigned long prevPIDControlExecute;
boolean autoPrint; 


//current PID control wieghts  (will be adjusted for tuning the PID controller)
float KP;
float KI;
float KD;
//current PID control values 
float P;
float I;
float D;

unsigned long lastPrint;

void conditionalPrint(){
  unsigned long currTime = millis();
  if (autoPrint && (currTime - lastPrint) > AUTO_PRINT_FREQ){
    lastPrint = currTime;
    printState();
  }
}

void printState(){
      // Print out current values 
    Serial.println("currPWM:  " +String(currPWM) + "   SetPoint: "+ String(currSetPoint));
    Serial.println(" P: " +String(P) + "    I: " +String(I) + "    D: " +String(D));
    Serial.println("KP: " +String(KP) +"   KI: " +String(KI) +"   KD: " +String(KD));
}

void executeModeOff(){
  setCoilPWM(0);
  readHallVal();
  
  //if we are not too close anymore go back into idle mode 
  if (currHallVal >= MIN_CONTROL_DISTANCE){
   setMode(MODE_IDLE);
  } 
}

void executeModeIdle(){
  
  readHallVal();
  //change to PID control if in the init interval 
  if ((currHallVal < INIT_CONTROL_INTERVAL_MAX) && (currHallVal > INIT_CONTROL_INTERVAL_MIN)){
    setMode(MODE_PID_CONTROL);
  }
  
}

void executeModePIDControl(){
  readHallVal();

  //if too far away then put the system back into idle and wait for the magnet to
  //be place back in range
  if (currHallVal > MAX_CONTROL_DISTANCE){
    setMode(MODE_IDLE);
   return; 
  }
  //if system is too close turn it off 
  if (currHallVal < MIN_CONTROL_DISTANCE){
   setMode(MODE_OFF);
   return; 
  } 
  
  unsigned long currTime = millis();
 
  //update the PWM at the CONTROL_UPDATE_FREQ
  if( currTime > (prevPIDControlExecute + CONTROL_UPDATE_FREQ)){
     prevPIDControlExecute = currTime;
     
     //P is the current error of the current position vs where we want the magnet to be 
     P = currSetPoint - currHallVal; 
     
     //I is a cumelative error, make sure it doesnt get too high. 
     I += P;
     I = constrain(I, -MAX_I, MAX_I); 
     
     //D is the derivative term ie the difference between the current position and the previous one 
     D = prevHallVal - currHallVal;
     
     int newPWM = currPWMMidpoint + (int)(KP*P) + (int)(KI*I) + (int)(KD*D);
     
     //make sure we have a valid PWM value 
     newPWM = constrain( newPWM, 0, MAX_PWM);
     
     //set system PWM to new value 
     setCoilPWM(newPWM);
   }
}

void initPWMTimer(){
  pinMode(PIN_COIL, OUTPUT);
   // Timer 2 register: WGM20 sets PWM phase correct mode, COM2x1 sets the PWM out to channels A and B.
   TCCR2A = 0;
   TCCR2A = _BV(COM2A1) | _BV(COM2B1) | _BV(WGM20);
   // Set the prescaler to 8, the PWM freq is 16MHz/255/2/<prescaler>
   TCCR2B = 0;
   TCCR2B = _BV(CS21);
}


void loop(){
     // User commands.
   if( Serial.available() ) 
   {   
      processUserInput(); 
   }
  
  //auto print if set
  conditionalPrint();
  
  //execute proper mode
  switch( currMode )
  {
    case MODE_OFF:
        executeModeOff();
        Serial.println("OFF");
        break;
    case MODE_IDLE:
        executeModeIdle();
        Serial.println("ID");
        break;
    case MODE_PID_CONTROL:
        executeModePIDControl();
        Serial.println("EX");
        break; 
    default:
        break;
  }  
}

void processUserInput(){
  int input = Serial.read();
  switch(input){
    case 'p':
      KP -= ADJ_KP;
      break;
    case 'P':
      KP += ADJ_KP;
      break;
    case 'i':
      KI-= ADJ_KI;
      break;
    case 'I':
      KI += ADJ_KI;
      break;
    case 'd':
      KD -= ADJ_KD;
      break;
    case 'D':
      KD += ADJ_KD;
      break;
    case 's':
      currSetPoint -= 1;
      break;
    case 'S':
      currSetPoint += 1;
      break;
    case 'm':
      currPWMMidpoint -= 1;
      break;
    case 'M':
      currPWMMidpoint += 1;
      break;
    case 'o':
      printState();
      break;
    case 'O':
      autoPrint = !autoPrint;
      break;
    default: 
         break;
  }
    
}

void readHallVal(){
  prevHallVal = currHallVal;
  currHallVal = analogRead(PIN_HALL_SENSOR);
}

void resetToIdle(){
  //current PID control values 
  P = 0;
  I = 0;
  D = 0;
  
  setMode(MODE_IDLE);
  currPWM = IDLE_PWM;
  setCoilPWM(currPWM);
  currPWMMidpoint = INIT_PWM_MIDPOINT; 
  currSetPoint = INIT_SET_POINT;
  currHallVal = analogRead(PIN_HALL_SENSOR);
  prevHallVal = currHallVal;
  prevPIDControlExecute = millis();
}

void setCoilPWM(int setValue){
  currPWM = setValue;
  OCR2A = currPWM;
}

void setMode(int mode){
    switch(mode)
    {
      case MODE_OFF:
          currMode = MODE_OFF;
          digitalWrite(PIN_STATE_MODE_OFF,HIGH);
          digitalWrite(PIN_STATE_MODE_IDLE,LOW);
          digitalWrite(PIN_STATE_MODE_PID_CONTROL,LOW);
          break;
      case MODE_IDLE:
          resetToIdle();
          digitalWrite(PIN_STATE_MODE_OFF,LOW);
          digitalWrite(PIN_STATE_MODE_IDLE,HIGH);
          digitalWrite(PIN_STATE_MODE_PID_CONTROL,LOW);
          break;
      case MODE_PID_CONTROL:
          currMode = MODE_PID_CONTROL;
          digitalWrite(PIN_STATE_MODE_OFF,LOW);
          digitalWrite(PIN_STATE_MODE_IDLE,LOW);
          digitalWrite(PIN_STATE_MODE_PID_CONTROL,HIGH);
          break; 
      default:
          break;
    }
}

void setup(){
  Serial.begin(9600);
  pinMode(PIN_STATE_MODE_OFF, OUTPUT);
  pinMode(PIN_STATE_MODE_IDLE, OUTPUT);
  pinMode(PIN_STATE_MODE_PID_CONTROL, OUTPUT);
  
  lastPrint = millis();
  autoPrint = false; 
  
  initPWMTimer();
 
  resetToIdle();

  //current PID control wieghts  (will be adjusted for tuning the PID controller)
  KP = INIT_KP;
  KI = INIT_KI;
  KD = INIT_KD;
}





