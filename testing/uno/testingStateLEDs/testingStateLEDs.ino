//current mode indicator lights red, yellow, and green respectively
const int PIN_STATE_MODE_OFF = 4;
const int PIN_STATE_MODE_IDLE = 5;
const int PIN_STATE_MODE_PID_CONTROL = 6;
void setup() {
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(PIN_STATE_MODE_OFF,HIGH);
  digitalWrite(PIN_STATE_MODE_IDLE,LOW);
  digitalWrite(PIN_STATE_MODE_PID_CONTROL,LOW);
  delay(500);
  
    digitalWrite(PIN_STATE_MODE_OFF,LOW);
  digitalWrite(PIN_STATE_MODE_IDLE,HIGH);
  digitalWrite(PIN_STATE_MODE_PID_CONTROL,LOW);
  delay(500);

  digitalWrite(PIN_STATE_MODE_OFF,LOW);
  digitalWrite(PIN_STATE_MODE_IDLE,LOW);
  digitalWrite(PIN_STATE_MODE_PID_CONTROL,HIGH);
  delay(500);


}
