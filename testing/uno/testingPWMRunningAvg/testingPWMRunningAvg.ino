
const int PIN_COIL = 11; // Pins 3 and 11 are connected to Timer 2.
const int PIN_HALL_SENSOR = A0;

const int INIT_PWM = 0; 

const int delayMiliSec = 100;

String readString;

unsigned long lastPrint;

long compoundedSample =0;
long numSample = 0; 

int currPWM;

//the most recent Hall value read in, and the value before that. 
float currHallVal;
float prevHallVal;

//number of moveign mean values to record
const int NUM_MM_VALS = 5;
// Set timer1_counter to the correct value for our interrupt interval
const int MM_TIMER_PRELOAD = 65516;   // preload timer 2^16-16MHz/8/100KHz 


//next moveing average val to be overwritten 
volatile int nextMMToRead = 0;
//all of the most recent moveing average vals 
volatile int MMVals[NUM_MM_VALS];


void setupCoilPWM()
{  
   // Setup the timer 2 as Phase Correct PWM, 3921 Hz.
   pinMode(3, OUTPUT);
   pinMode(11, OUTPUT);
   // Timer 2 register: WGM20 sets PWM phase correct mode, COM2x1 sets the PWM out to channels A and B.
   TCCR2A = 0;
   TCCR2A = _BV(COM2A1) | _BV(COM2B1) | _BV(WGM20);
   // Set the prescaler to 8, the PWM freq is 16MHz/255/2/<prescaler> ie 3921.16Hz
     TCCR2B = 0x02;
}

void setupMMInterupt(){

  // initialize timer1 
  noInterrupts();           // disable all interrupts
  TCCR1A = 0;
  TCCR1B = 0;
  
  TCNT1 = MM_TIMER_PRELOAD;   // preload timer
  TCCR1B = 0x02;    // 8 prescaler 
  TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt
  interrupts();             // enable all interrupts
}

ISR(TIMER1_OVF_vect)        // interrupt service routine 
{
  TCNT1 = MM_TIMER_PRELOAD;   // preload timer
  MMVals[nextMMToRead] = analogRead(PIN_HALL_SENSOR);
  //update next MM tht should be read in 
  nextMMToRead++; 
  if (nextMMToRead >= NUM_MM_VALS) nextMMToRead = 0; 
}

void writeCoilPWM(uint8_t pin, int val)
{
  currPWM = val;
  OCR2A = currPWM;
}

void setup()
{
   setupCoilPWM();
   
   writeCoilPWM(PIN_COIL, INIT_PWM);
   currPWM = INIT_PWM; 
   
   // initialize serial communication at 9600 bits per second:
   Serial.begin(9600);
   lastPrint = millis();
   
}

void loop(){
   digitalWrite(4,HIGH);
   digitalWrite(5,HIGH);
   digitalWrite(6,HIGH);
    // User commands.
   if( Serial.available() ) 
   { 
       while (Serial.available()) {
          char c = Serial.read();  //gets one byte from serial buffer
          readString += c; //makes the string readString
          delay(2);  //slow looping to allow buffer to fill with next character
        } 
        
        if (readString.length() >0) {
          currPWM = readString.toInt();  //convert readString into a number
        }
        readString=""; //empty for next input
         
       writeCoilPWM(PIN_COIL, currPWM);
       Serial.println("set PWM: "+ String(currPWM));
   }  

  unsigned long currTime = millis();
  if ((currTime - lastPrint) > delayMiliSec){
    lastPrint = currTime;
    readHallVal();
    // print out the value you read:
    Serial.println(String(currPWM) + "       " + String(currHallVal));
  }
  
}

  void readHallVal(){
  prevHallVal = currHallVal;
  //hall value is the average of the current MMVals 
  float sum =0;
  for (int i; i < NUM_MM_VALS; i++){
    sum += MMVals[i];
  }
  currHallVal = sum/NUM_MM_VALS;
  
}
