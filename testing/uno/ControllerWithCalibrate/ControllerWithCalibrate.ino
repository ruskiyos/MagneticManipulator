const int PIN_COIL = 11; // Pins 3 and 11 are connected to Timer 2.
const int PIN_HALL_SENSOR = A0;

//current mode indicator lights red, yellow, and green respectively
const int PIN_STATE_MODE_OFF = 4;
const int PIN_STATE_MODE_IDLE = 5;
const int PIN_STATE_MODE_PID_CONTROL = 6;

//time between bewteen exectutions of a check and update of the current system state ms 
const int CONTROL_UPDATE_FREQ = 1;

//time to wait before reading calibration value after setting PWM
//note total calibration time will be CALIBRATE_WAIT_TIME * 255 
const int CALIBRATE_WAIT_TIME = 30;

const int AUTO_PRINT_FREQ = 1000;

// Minimum magnetic field when in idle mode (needed to properly orientate the magnet).
const int IDLE_PWM = 10;
const int MAX_PWM = 255; 
//The field strength (as read in on the hall sensor) critical point where a magnet is 
//lifted at the init setpoint. Used for calibration and determineing init PWMMidpoint 
const int INIT_PWM_MIDPOINT_FIELD_STR = 40;
const int INIT_SET_POINT = 50; 

//system will change state from IDLE to PID_CONTROL when the magnet enters this interval
const int INIT_CONTROL_INTERVAL_MAX = 80;
const int INIT_CONTROL_INTERVAL_MIN = 60;

//The range that the system will countinue to operate in control mode for 
const int MIN_CONTROL_HALL_READING = 5;
const int MAX_CONTROL_HALL_READING = 420;

//the maximume cumilative error the sytem should allow in its calculations
const int MAX_I = 1000;

// State machine constants.
const byte MODE_OFF = 0;
const byte MODE_CALIBRATE = 1;
const byte MODE_IDLE = 2; 
const byte MODE_PID_CONTROL = 3;

//PID control wieghts 
const float INIT_KP = 2.1;
const float INIT_KI = 0.001;
const float INIT_KD = 23.6;

//PID control wieght adjustment amounts used for tuning PID control algorythm
const float ADJ_KP = .1;
const float ADJ_KI = .001;
const float ADJ_KD = .1;

//the current state machine mode
int currMode;
//the PWM that is currently being applied
int currPWM;
int currPWMMidpoint;
int currSetPoint;
//the most recent Hall value read in, and the value before that. 
int currHallVal;
int prevHallVal;
//array of Hall values associate with given PWMs
int calibrationVals[MAX_PWM +1];
int PWMToCalibrate;

//whether to autoprint system info a regular rate
boolean autoPrint; 

//current PID control wieghts  (will be adjusted for tuning the PID controller)
float KP;
float KI;
float KD;
//current PID control values 
float P;
float I;
float D;

//last time a pertinent function was executed, what this function was depends on mode
unsigned long prevExecute;
unsigned long lastPrint;

void conditionalPrint(){
  unsigned long currTime = millis();
  if (autoPrint && (currTime - lastPrint) > AUTO_PRINT_FREQ){
    lastPrint = currTime;
    printState();
  }
}

void printState(){
    // Print out current values 
    Serial.println(modeToSting());
    Serial.println("currPWM:  " +String(currPWM) + "   SetPoint: "+ String(currSetPoint)+ "   Hall: "+ String(currHallVal));
    Serial.println(" P: " +String(P) + "    I: " +String(I) + "    D: " +String(D));
    Serial.println("KP: " +String(KP) +"   KI: " +String(KI) +"   KD: " +String(KD));
    Serial.println("");
}

String modeToSting(){
    switch( currMode )
  {
    case MODE_OFF:
        executeModeOff();
        return "OFF";
    case MODE_CALIBRATE:
        return "CALIBRATE"; 
    case MODE_IDLE:
        return "IDLE";
    case MODE_PID_CONTROL:
        return "CONTROL";
    default:
        break;
  }  
}

void executeModeOff(){
  setCoilPWM(0);
  readHallVal();
  
  //if we are not too close anymore go back into idle mode 
  if (currHallVal <= MAX_CONTROL_HALL_READING){
   setMode(MODE_IDLE);
  } 
}

void executeModeCalibrate(){
  //update the PWM at the CONTROL_UPDATE_FREQ
  if( millis() > (prevExecute + CALIBRATE_WAIT_TIME)){
    
    //read and store value 
    readHallVal();
    calibrationVals[PWMToCalibrate] = currHallVal;
    Serial.println("PWM: "+ String(PWMToCalibrate)+" Hall: "+String(calibrationVals[PWMToCalibrate]));
    
    //if our PWMToCalibrate is greater than our MAX_PWM then we are done calibrateing 
    if (PWMToCalibrate >= MAX_PWM){
        Serial.println("Done CALIBRATE");
         setMode(MODE_IDLE);
         currPWMMidpoint = findBestCalibratePWM(INIT_PWM_MIDPOINT_FIELD_STR);
    }
    else{
      //set up read of next value 
      
      PWMToCalibrate ++;
      setCoilPWM(PWMToCalibrate); 
      prevExecute = millis();
    }
  }
}

void executeModeIdle(){
  readHallVal();
  //change to PID control if in the init interval 
  if ((currHallVal < INIT_CONTROL_INTERVAL_MAX) && (currHallVal > INIT_CONTROL_INTERVAL_MIN)){
    setMode(MODE_PID_CONTROL);
  }
}

void executeModePIDControl(){
  readHallVal();

  //if too far away then put the system back into idle and wait for the magnet to
  //be place back in range
  if (currHallVal < MIN_CONTROL_HALL_READING){
    setMode(MODE_IDLE);
   return; 
  }
  //if system is too close turn it off 
  if (currHallVal > MAX_CONTROL_HALL_READING){
   setMode(MODE_OFF);
   return; 
  } 
  
  unsigned long currTime = millis();
 
  //update the PWM at the CONTROL_UPDATE_FREQ
  if( currTime > (prevExecute + CONTROL_UPDATE_FREQ)){
     prevExecute = currTime;
     
     //P is the current error of the current position vs where we want the magnet to be 
     P = currSetPoint - currHallVal; 
     
     //I is a cumelative error, make sure it doesnt get too high. 
     I += P;
     I = constrain(I, -MAX_I, MAX_I); 
     
     //D is the derivative term ie the difference between the current position and the previous one 
     D = prevHallVal - currHallVal;
     
     int newPWM = currPWMMidpoint + (int)(KP*P) + (int)(KI*I) + (int)(KD*D);
     
     //make sure we have a valid PWM value 
     newPWM = constrain( newPWM, 0, MAX_PWM);
     
     //set system PWM to new value 
     setCoilPWM(newPWM);
   }
}

//return the PWM best which will produce the given field (without a magnet), at the given calibration
int findBestCalibratePWM(int fieldStr){
  //assume 0 PWM has best error
  int closestPWM = 0; 
  int leastError = abs(calibrationVals[0] - fieldStr);
  //if any other PWM is better save that one 
  for( int i = 1; i <= MAX_PWM; i++){
    int thisError = abs(calibrationVals[i] - fieldStr); 
    if (thisError < leastError ){
      closestPWM = i;
      leastError = thisError; 
    }
  }
  
  return closestPWM; 
}

void initPWMTimer(){
  pinMode(PIN_COIL, OUTPUT);
   // Timer 2 register: WGM20 sets PWM phase correct mode, COM2x1 sets the PWM out to channels A and B.
   TCCR2A = 0;
   TCCR2A = _BV(COM2A1) | _BV(COM2B1) | _BV(WGM20);
   // Set the prescaler to 8, the PWM freq is 16MHz/255/2/<prescaler>
   TCCR2B = 0x03;
}


void loop(){
     // User commands.
   if( Serial.available() ) 
   {   
      processUserInput(); 
   }
  
  //auto print if set
  conditionalPrint();
  
  //execute proper mode
  switch( currMode )
  {
    case MODE_OFF:
        executeModeOff();
        break;
    case MODE_CALIBRATE:
        executeModeCalibrate();
        break; 
    case MODE_IDLE:
        executeModeIdle();
        break;
    case MODE_PID_CONTROL:
        executeModePIDControl();
        break; 
    default:
        break;
  }  
}

void processUserInput(){
  int input = Serial.read();
  switch(input){
    case 'p':
      KP -= ADJ_KP;
      break;
    case 'P':
      KP += ADJ_KP;
      break;
    case 'i':
      KI-= ADJ_KI;
      break;
    case 'I':
      KI += ADJ_KI;
      break;
    case 'd':
      KD -= ADJ_KD;
      break;
    case 'D':
      KD += ADJ_KD;
      break;
    case 's':
      currSetPoint -= 1;
      break;
    case 'S':
      currSetPoint += 1;
      break;
    case 'm':
      currPWMMidpoint -= 1;
      break;
    case 'M':
      currPWMMidpoint += 1;
      break;
    case 'o':
      printState();
      break;
    case 'O':
      autoPrint = !autoPrint;
      break;
    case 'c':
      setMode(MODE_CALIBRATE);
      break;
    default: 
         break;
  }
    
}

void readHallVal(){
  prevHallVal = currHallVal;
  currHallVal = analogRead(PIN_HALL_SENSOR)-calibrationVals[currPWM];
}

void resetToIdle(){
  //current PID control values 
  P = 0;
  I = 0;
  D = 0;
  
  currMode = MODE_IDLE;
  currPWM = IDLE_PWM;
  setCoilPWM(currPWM);
  currPWMMidpoint = INIT_PWM_MIDPOINT_FIELD_STR; 
  currSetPoint = INIT_SET_POINT;
  readHallVal();
  prevHallVal = currHallVal;
  prevExecute = millis();
}

void setCoilPWM(int setValue){
  currPWM = setValue;
  OCR2A = currPWM;
}

void setMode(int mode){
    switch(mode)
    {
      case MODE_OFF:
          Serial.println("set mode OFF");
          currMode = MODE_OFF;
          digitalWrite(PIN_STATE_MODE_OFF,HIGH);
          digitalWrite(PIN_STATE_MODE_IDLE,LOW);
          digitalWrite(PIN_STATE_MODE_PID_CONTROL,LOW);
          break;
      case MODE_CALIBRATE:
          Serial.println("set mode CALIBRATE");
          currMode = MODE_CALIBRATE;
          PWMToCalibrate = 0;
          prevExecute = millis();
          digitalWrite(PIN_STATE_MODE_OFF,HIGH);
          digitalWrite(PIN_STATE_MODE_IDLE,HIGH);
          digitalWrite(PIN_STATE_MODE_PID_CONTROL,HIGH);
          break; 
      case MODE_IDLE:
          Serial.println("set mode IDLE");
          resetToIdle();
          digitalWrite(PIN_STATE_MODE_OFF,LOW);
          digitalWrite(PIN_STATE_MODE_IDLE,HIGH);
          digitalWrite(PIN_STATE_MODE_PID_CONTROL,LOW);
          break;
      case MODE_PID_CONTROL:
          Serial.println("set mode CONTROL");
          currMode = MODE_PID_CONTROL;
          digitalWrite(PIN_STATE_MODE_OFF,LOW);
          digitalWrite(PIN_STATE_MODE_IDLE,LOW);
          digitalWrite(PIN_STATE_MODE_PID_CONTROL,HIGH);
          break; 
      default:
          break;
    }
}

void setup(){
  Serial.begin(9600);
  Serial.println("SETUP");
  pinMode(PIN_STATE_MODE_OFF, OUTPUT);
  pinMode(PIN_STATE_MODE_IDLE, OUTPUT);
  pinMode(PIN_STATE_MODE_PID_CONTROL, OUTPUT);
  
  lastPrint = millis();
  autoPrint = false; 
  
  initPWMTimer();
  
  // set the PWM to zero and wait a bit to make sure Moveing mean array is populated
  setCoilPWM(0);
  delay(1); 
  
  setMode(MODE_CALIBRATE);

  //current PID control wieghts  (will be adjusted for tuning the PID controller)
  KP = INIT_KP;
  KI = INIT_KI;
  KD = INIT_KD;
}





