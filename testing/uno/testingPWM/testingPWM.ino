
const int PIN_COIL = 11; // Pins 3 and 11 are connected to Timer 2.
const int PIN_HALL_SENSOR = 1;

const int INIT_PWM = 0; 

const int delayMiliSec = 100;

String readString;

unsigned long lastPrint;

long compoundedSample =0;
long numSample = 0; 

int currPWM;

void setupCoilPWM()
{  
   // Setup the timer 2 as Phase Correct PWM, 3921 Hz.
   pinMode(3, OUTPUT);
   pinMode(11, OUTPUT);
   // Timer 2 register: WGM20 sets PWM phase correct mode, COM2x1 sets the PWM out to channels A and B.
   TCCR2A = 0;
   TCCR2A = _BV(COM2A1) | _BV(COM2B1) | _BV(WGM20);
   // Set the prescaler to 8, the PWM freq is 16MHz/255/2/<prescaler>
//   TCCR2B = 0;
//   TCCR2B = _BV(CS21);
     TCCR2B = 0x02
     ;
}

void writeCoilPWM(uint8_t pin, int val)
{
   if( pin == 3 )
   {
      OCR2B = val;
   }
   else if( pin == 11 )
   {
      OCR2A = val;
   }
   else
   {
      OCR2A = 0;
      OCR2B = 0;
   }
}

void setup()
{
   setupCoilPWM();
   
   writeCoilPWM(PIN_COIL, INIT_PWM);
   currPWM = INIT_PWM; 
   
   // initialize serial communication at 9600 bits per second:
   Serial.begin(9600);
   lastPrint = millis();
   
}

void loop(){
   digitalWrite(4,HIGH);
   digitalWrite(5,HIGH);
   digitalWrite(6,HIGH);
    // User commands.
   if( Serial.available() ) 
   { 
       while (Serial.available()) {
          char c = Serial.read();  //gets one byte from serial buffer
          readString += c; //makes the string readString
          delay(2);  //slow looping to allow buffer to fill with next character
        } 
        
        if (readString.length() >0) {
          currPWM = readString.toInt();  //convert readString into a number
        }
        readString=""; //empty for next input
         
       writeCoilPWM(PIN_COIL, currPWM);
       Serial.println("set PWM: "+ String(currPWM));
   }  
  compoundedSample += analogRead(A0);
  numSample ++;
  unsigned long currTime = millis();
  if ((currTime - lastPrint) > delayMiliSec){
    lastPrint = currTime;
    // read the input on analog pin 0:
    int sensorValue = int(compoundedSample/numSample);
    compoundedSample = 0;
    numSample = 0; 
    //int sensorValue = analogRead(A0);
    // print out the value you read:
    Serial.println(String(currPWM) + "       " + String(sensorValue));
  }
  
}
