// Import serial library
import processing.serial.*;

// Serial port object
Serial myPort;

String val;

// pre-handshake
boolean handshake = false;

void setup() {
  // Canvas size 200x200 px
  size(200,200);
  
  println("Serial Devices:");
  for (int ix = 0; ix < Serial.list().length; ix++) {
    println(Serial.list()[ix]);
  }
  
  // Initialize serial port and set baud rate to 9600
  myPort = new Serial(this, Serial.list()[0], 9600);
  myPort.bufferUntil('\n');
}

void draw() {
  // empty because of serial event listener
}

void serialEvent(Serial myPort) {
  // store incoming data as string "\n" delimited
  val = myPort.readStringUntil('\n');
  
  if (val != null) {
    // trim whitespace
    val = trim(val);
    println(val);
    
    if (handshake == false) {
      if (val.equals("H")) {
        myPort.clear();
        handshake = true;
        myPort.write("A");
        println("contact");
      } else {
        println(val);
      }
    }
  }
}

