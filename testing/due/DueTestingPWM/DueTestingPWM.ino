
#include "pwm01.h"
const int PIN_COIL = 9; 
const int PIN_HALL_SENSOR = A0;

const float INIT_PWM = 0.5; 
const uint32_t PWM_FREQ = 10000;

const int delayMiliSec = 1000;
const int MAX_PWM_COUNT = 65535; // 2^16 - 1
String readString;

unsigned long lastPrint;

float currPWM;

void setupCoilPWM(Tc *tc, uint32_t channel)
{  
    // Set PWM Resolution
    pwm_set_resolution(16);  

    pwm_setup( PIN_COIL, PWM_FREQ, 1);  // Pin 9 freq set to "pwm_freq2" on clock B
      
    writeCoilPWM(INIT_PWM);

}

void writeCoilPWM(float dutyPercent)
{
    if (dutyPercent < 0 ){
      pwm_write_duty( PIN_COIL, ((uint32_t)0));
    }
    else if ( dutyPercent > 1){
      pwm_write_duty( PIN_COIL, ((uint32_t)MAX_PWM_COUNT));
    }
    else{
      pwm_write_duty( PIN_COIL, (uint32_t)(MAX_PWM_COUNT*dutyPercent));
    }
}

void setup()
{
   // initialize serial communication at 9600 bits per second:
   Serial.begin(115200);
  
   setupCoilPWM(TC0, 0);
   
   writeCoilPWM(INIT_PWM);
   currPWM = INIT_PWM; 
   
   lastPrint = millis();
   
}

void loop(){

    // User commands.
   if( Serial.available() ) 
   { 
       while (Serial.available()) {
          char c = Serial.read();  //gets one byte from serial buffer
          readString += c; //makes the string readString
          delay(2);  //slow looping to allow buffer to fill with next character
        } 
        
        if (readString.length() >0) {
          char carray[readString.length() + 1]; //determine size of the array
          readString.toCharArray(carray, sizeof(carray)); //put readStringinto an array
          currPWM = atof(carray);  //convert readString into a number
        }
        readString=""; //empty for next input
       Serial.println("set PWM: "+ String(currPWM));
       writeCoilPWM(currPWM);
   }  
//  compoundedSample += analogRead(A0);
//  numSample ++;
  unsigned long currTime = millis();
  if ((currTime - lastPrint) > delayMiliSec){
    lastPrint = currTime;
    // read the input on analog pin 0:
    //int sensorValue = analogRead(A0);
    // print out the value you read:
    Serial.println(String(currPWM) + "       " + String(analogRead(PIN_HALL_SENSOR)));
  }
  
}
