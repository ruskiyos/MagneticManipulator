
#include "pwm01.h"
const int PIN_COIL = 9; 
const int PIN_COIL_HALL_SENSOR = A8;

const float INIT_PWM = 0.5;
const float ADJ_PWM = 0.001;
const int adjDelayMiliSec = 50;

const uint32_t PWM_FREQ = 10000;

const int MM_FREQ = 100000;

const int delayMiliSec = 1000;
const int MAX_PWM_COUNT = 65535; // 2^16 - 1

String readString;

unsigned long lastPrint;
unsigned long lastAdj;

long compoundedSample =0;
volatile int numSample = 0; 

float currPWM;

//the most recent Hall value read in, and the value before that. 
float currHallVal;
float prevHallVal;

//number of moveign mean values to record
const int NUM_MM_VALS = 10;
// time between MM Samples in ms 
const int MM_PERIOD = 10;  

// Timer and Channel
Tc *pwmTc;
Tc *mmTc;
uint32_t pwmChannel;
uint32_t rcPWM;
uint32_t rcMM;


//next moveing average val to be overwritten 
volatile int nextMMToRead = 0;
//all of the most recent moveing average vals 
volatile int MMVals[NUM_MM_VALS];
volatile int MMSum = 0;

boolean incrementPWM = false; 


void setupCoilPWM()
{  
    // Set PWM Resolution
    pwm_set_resolution(16);  

    pwm_setup( PIN_COIL, PWM_FREQ, 1);  // Pin 9 freq set to "pwm_freq" on clock B
      
    writeCoilPWM(INIT_PWM);
}

void ADC_Handler (void) {
  TC_GetStatus(TC1, 0);
    //get rid of old val
  MMSum -= MMVals[nextMMToRead];
  
    //wait untill all 8 ADCs have finished thier convertion. 
  while(!(((ADC->ADC_ISR & ADC_ISR_EOC7) && (ADC->ADC_ISR & ADC_ISR_EOC6) && (ADC->ADC_ISR & ADC_ISR_EOC5) && (ADC->ADC_ISR & ADC_ISR_EOC4)
   && (ADC->ADC_ISR & ADC_ISR_EOC3) && (ADC->ADC_ISR & ADC_ISR_EOC2) && (ADC->ADC_ISR & ADC_ISR_EOC1) && (ADC->ADC_ISR & ADC_ISR_EOC0))));
  
  //add all of adc values up and bit shit right three times to devide by 8
  //typcast to unsigned int so leading bits are shifted in as 0s
  int reading = ((unsigned int)(*(ADC->ADC_CDR+7)+*(ADC->ADC_CDR+6)+*(ADC->ADC_CDR+5)+*(ADC->ADC_CDR+4)+*(ADC->ADC_CDR+3)+*(ADC->ADC_CDR+2)+
  *(ADC->ADC_CDR+1)+*(ADC->ADC_CDR+0))) >>3;
  
  MMVals[nextMMToRead] = reading;
  MMSum += reading;
  //update next MM tht should be read in 
  nextMMToRead++; 
  nextMMToRead = nextMMToRead % NUM_MM_VALS; 
}

void setupADC ()
{
  NVIC_EnableIRQ (ADC_IRQn) ;   // enable ADC interrupt vector
  ADC->ADC_IDR = 0xFFFFFFFF ;   // disable interrupts
  ADC->ADC_IER = 0x80 ;         // enable AD7 End-Of-Conv interrupt (Arduino pin A0)
  ADC->ADC_CHDR = 0xFFFF ;      // disable all channels
  ADC->ADC_CHER = 0xFFF ;        // ch7:A0 ch6:A1 ch5:A2 ch4:A3 ch3:A4 ch2:A5 ch1:A6 ch0:A7
  ADC->ADC_CGR = 0x15555555 ;   // All gains set to x1
  ADC->ADC_COR = 0x00000000 ;   // All offsets off
  
  ADC->ADC_MR = (ADC->ADC_MR & 0xFFFFFFF0) | (1 << 1) | ADC_MR_TRGEN ;  // 1 = trig source TIO from TC0
}

void setupMMTimer(){
  pmc_enable_periph_clk (TC_INTERFACE_ID + 0*3+0) ;  // clock the TC0 channel 0

  TcChannel * t = &(TC0->TC_CHANNEL)[0] ;    // pointer to TC0 registers for its channel 0
  t->TC_CCR = TC_CCR_CLKDIS ;  // disable internal clocking while setup regs
  t->TC_IDR = 0xFFFFFFFF ;     // disable interrupts
  t->TC_SR ;                   // read int status reg to clear pending
  t->TC_CMR = TC_CMR_TCCLKS_TIMER_CLOCK1 |   // use TCLK1 (prescale by 2, = 42MHz)
              TC_CMR_WAVE |                  // waveform mode
              TC_CMR_WAVSEL_UP_RC |          // count-up PWM using RC as threshold
              TC_CMR_EEVT_XC0 |     // Set external events from XC0 (this setup TIOB as output)
              TC_CMR_ACPA_CLEAR | TC_CMR_ACPC_CLEAR |
              TC_CMR_BCPB_CLEAR | TC_CMR_BCPC_CLEAR ;
  
  t->TC_RC =  VARIANT_MCK/2/MM_FREQ;     // counter resets on RC, so sets period in terms of 42MHz clock
  t->TC_RA =  (VARIANT_MCK/2/MM_FREQ)/2 ;     // roughly square wave
  t->TC_CMR = (t->TC_CMR & 0xFFF0FFFF) | TC_CMR_ACPA_CLEAR | TC_CMR_ACPC_SET ;  // set clear and set from RA and RC compares
  
  t->TC_CCR = TC_CCR_CLKEN | TC_CCR_SWTRG ;  // re-enable local clocking and switch to hardware trigger source.
}

void writeCoilPWM(float dutyPercent)
{
    if (dutyPercent < 0 ){
      pwm_write_duty( PIN_COIL, ((uint32_t)0));
    }
    else if ( dutyPercent > 1){
      pwm_write_duty( PIN_COIL, ((uint32_t)MAX_PWM_COUNT));
    }
    else{
      pwm_write_duty( PIN_COIL, (uint32_t)(MAX_PWM_COUNT*dutyPercent));
    }
}

void setup()
{
   Serial.begin(115200);
   setupCoilPWM();
   
   writeCoilPWM(INIT_PWM);
   currPWM = INIT_PWM;
   setupADC (); 
   setupMMTimer();
 
   
   // initialize serial communication at 9600 bits per second:
   lastPrint = millis();
   digitalWrite(4,HIGH);
   digitalWrite(5,HIGH);
   digitalWrite(6,HIGH);
   
}

void loop(){
    //Serial.println(numSample);
    //numSample++;
    // User commands.
   if( Serial.available() ) 
   { 
       while (Serial.available()) {
          char c = Serial.read();  //gets one byte from serial buffer
          if (c == 'i'){
            incrementPWM = !incrementPWM;
            Serial.println("set increment: "+ String(incrementPWM)); 
            return; 
          } 
          readString += c; //makes the string readString
          delay(2);  //slow looping to allow buffer to fill with next character
        } 
        
        if (readString.length() >0) {
          char carray[readString.length() + 1]; //determine size of the array
          readString.toCharArray(carray, sizeof(carray)); //put readStringinto an array
          currPWM = atof(carray);  //convert readString into a number
        }
        readString=""; //empty for next input
       Serial.println("set PWM: "+ String(currPWM));
       writeCoilPWM(currPWM);
   }   

  unsigned long currTime = millis();
  if ((currTime - lastPrint) > delayMiliSec){
    lastPrint = currTime;
    readHallVal();
    // print out the value you read:
    Serial.println(String(currPWM) + "       " + String(currHallVal)+ "       " + String(analogRead(PIN_COIL_HALL_SENSOR)));
  }
  
  if (incrementPWM && (currTime - lastAdj) > adjDelayMiliSec){
    lastAdj = currTime;
    currPWM = (currPWM + ADJ_PWM);
    if (currPWM >1) currPWM = 0;
    writeCoilPWM(currPWM);
  }
}

  void readHallVal(){
  //save old hall value 
  prevHallVal = currHallVal;
  currHallVal = ((float)MMSum)/NUM_MM_VALS;
//  for (int i=0; i< NUM_MM_VALS;i++){
//    Serial.print(String(MMVals[i])+ ",");
//  }
//  Serial.println(")");

  
}

