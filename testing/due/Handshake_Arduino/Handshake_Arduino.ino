char val;  // Data received from serial port
String mesg;
boolean handshake = false;  

void setup() {
  
  Serial.begin(9600);
  
  pinMode(13, OUTPUT);
  
  // Don't continue until talking to
  // processing code
  establishContact();
}

// Loop in this method until Arduino
// begins to talk to processing code
void establishContact() {
  while(Serial.available() <= 0) {
    digitalWrite(13, HIGH);
    Serial.println("H");
    delay(500);
    digitalWrite(13, LOW);
    delay(50);
  }
  handshake = true;
  
}

void loop() {
  if (!handshake) {
    establishContact();
  }
  
  if (Serial.available() > 0) {
    val = Serial.read();
    mesg += val;
    digitalWrite(13, HIGH);
  } else {
    digitalWrite(13, LOW);
    if (mesg != ""){
      mesg.trim();
      Serial.println("Read mesg: " + mesg);
      mesg = "";
    } else {
      Serial.println("No value.");
    }
  }
  
  delay(500);
}
