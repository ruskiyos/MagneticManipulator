// File: MagneticManipulatorDue
// Authors:
//    Marley Rutkowski
//    John Olennikov

//================================================================================
//                                Program Constants
//================================================================================
// Enable to talk to Processing Code
const int REQUIRE_SERIAL = true;

const int PIN_COIL = 13;
const int PIN_HALL_SENSOR = A0;

//current mode indicator lights red, yellow, and green respectively
const int PIN_STATE_MODE_OFF = 53;
const int PIN_STATE_MODE_IDLE = 51;
const int PIN_STATE_MODE_PID_CONTROL = 49;

//time to wait before(miliSec) reading calibration value after setting PWM
const int CALIBRATE_WAIT_TIME = 10;
//time between samples for given calibrate (microsec)
//note should not be less than ((1/mm_FREQ)* NUM_MM_VALS)micros or vals may get used twice
const int CALIBRATE_SAMPLE_WAIT_TIME = 100;
const int CALIBRATE_PRECISION = 7;

const int AUTO_PRINT_FREQ = 1000;

//time between bewteen exectutions of a check and update of the current system state ms 
const int CONTROL_UPDATE_FREQ = 1;

//Frequency of the PWM pulse  
const int PWM_FREQ = 10000;
//2 because will use TIMER_CLK1, and another 2 becasue is UP/DOWN counter;
const uint32_t rcPWM = VARIANT_MCK/2/2/PWM_FREQ; 

const int MM_FREQ = 100000;
//number of moveign mean values to record
const int NUM_MM_VALS = 10;

// Minimum magnetic field when in idle mode (needed to properly orientate the magnet).
const float IDLE_PWM = .1;
const int MAX_PWM = 1; 

//The field strength (as read in on the hall sensor w/o magnet present) critical point where a 
//magnet is lifted at the init setpoint. Used for calibration and determineing init PWMMidpoint 
const float INIT_PWM_MIDPOINT_FIELD_STR = 183;
const float INIT_SET_POINT = 234; 

//system will change state from IDLE to PID_CONTROL when the magnet enters this interval
const int INIT_CONTROL_INTERVAL_MAX = 269;
const int INIT_CONTROL_INTERVAL_MIN = 150;

//The range that the system will countinue to operate in control mode for 
const int MIN_CONTROL_HALL_READING = 20;
const int MAX_CONTROL_HALL_READING = 3500;

//the maximume cumilative error the sytem should allow in its calculations
const int MAX_I = 1000;

// State machine constants.
const byte MODE_OFF = 0;
const byte MODE_CALIBRATE = 1;
const byte MODE_IDLE = 2; 
const byte MODE_PID_CONTROL = 3;

//PID control wieghts 
const float INIT_KP = 0.0001;
const float INIT_KI = 0;
const float INIT_KD = 0;

//PID control wieght adjustment amounts used for tuning PID control algorythm
const float ADJ_KP = .0001;
const float ADJ_KI = .0001;
const float ADJ_KD = .0001;



//================================================================================
//                              Variable Declarations
//================================================================================

//the current state machine mode
int currMode;
//the PWM that is currently being applied
float currPWM;
uint32_t currPWMCounterVal;
float currPWMMidpoint;
float currSetPoint;
//the most recent Hall value read in, and the value before that. 
float currHallVal;
float prevHallVal;

//array of Hall values associate with given PWMs
float calibrationVals[(int)(rcPWM/CALIBRATE_PRECISION) +1];
int PWMCountToCalibrate;
//time of prev calibration sample (micros)
unsigned long prevCalibrateSample;
unsigned int numCalibrateSample;
float calibrateSampleSum; 

//next moveing average val to be overwritten 
volatile int nextMMToRead = 0;
//all of the most recent moveing average vals 
volatile int MMVals[NUM_MM_VALS];
volatile int MMSum = 0;

// Timer and Channel
Tc *pwmTc;
Tc *mmTc;
uint32_t pwmChannel;
uint32_t rcMM;

//whether to autoprint system info a regular rate
boolean autoPrint; 

//current PID control wieghts  (will be adjusted for tuning the PID controller)
float KP;
float KI;
float KD;
//current PID control values 
float P;
float I;
float D;

//last time a pertinent function was executed, what this function was depends on mode
unsigned long prevExecute;
unsigned long lastPrint;


// Serial connection handshake status
boolean pHandShake = false;

//================================================================================
//                                 Print to Serial
//================================================================================

void conditionalPrint(){
  unsigned long currTime = millis();
  if (autoPrint && (currTime - lastPrint) > AUTO_PRINT_FREQ){
    lastPrint = currTime;
    printState();
  }
}

void printState(){
    // Print out current values 
    statusPrint(modeToSting());
    statusPrint("currPWM:  " +String(currPWM) + "   SetPoint: "+ String(currSetPoint)+ 
        "   Hall: "+ String(currHallVal));
    statusPrint(" P: " +String(P) + "    I: " +String(I) + "    D: " +String(D));
    statusPrint("KP: " +String(KP) +"   KI: " +String(KI) +"   KD: " +String(KD));
    statusPrint("");
}

String modeToSting(){
    switch( currMode )
  {
    case MODE_OFF:
        executeModeOff();
        return "OFF";
    case MODE_CALIBRATE:
        return "CALIBRATE"; 
    case MODE_IDLE:
        return "IDLE";
    case MODE_PID_CONTROL:
        return "CONTROL";
    default:
        break;
  }
}

//================================================================================
//                                  Execution Modes
//================================================================================

void executeModeOff(){
  setCoilPWM(0);
  readHallVal();
  
  //if we are not too close anymore go back into idle mode 
  if (currHallVal <= MAX_CONTROL_HALL_READING){
   setMode(MODE_IDLE);
  } 
}

void executeModeCalibrate(){
  
  //if enough time has passed take another sample 
  if  (micros() > (prevCalibrateSample + CALIBRATE_SAMPLE_WAIT_TIME)){
        readHallVal();
        //debugPrint(" currPWM: " +String(currPWM) + "    currHall: " +String(currHallVal));
        prevCalibrateSample = micros();
        numCalibrateSample += 1;
        calibrateSampleSum += currHallVal; 
  }
  
  //update the PWM at the CONTROL_UPDATE_FREQ
  if( millis() > (prevExecute + CALIBRATE_WAIT_TIME)){
    // if new calibration then init calibration vals to 0
    if (PWMCountToCalibrate == 0){
      for (int i=0; i <= (int)(rcPWM/CALIBRATE_PRECISION); i++)  calibrationVals[i] = 0;
    }

    calibrationVals[(int)(PWMCountToCalibrate/CALIBRATE_PRECISION)] = calibrateSampleSum/numCalibrateSample;
    Serial.println("#Samples: "+String(numCalibrateSample)+ " PWMCount: "+ String(PWMCountToCalibrate)+"/"+ String(rcPWM)+" ("+
    String(100*(float)PWMCountToCalibrate/(float)rcPWM)+"%) Hall: "+String(calibrationVals[(int)(PWMCountToCalibrate/CALIBRATE_PRECISION)]));
    numCalibrateSample = 0;
    calibrateSampleSum = 0; 
    //if our PWMCountToCalibrate is greater than our MAX_PWM then we are done calibrateing 
    if (PWMCountToCalibrate >= rcPWM){
        statusPrint("Done CALIBRATE");
         setMode(MODE_IDLE);
        // currPWMMidpoint = findBestCalibratePWM(INIT_PWM_MIDPOINT_FIELD_STR);
    }
    else{
      //set up read of next value 
      PWMCountToCalibrate +=CALIBRATE_PRECISION;
      if (PWMCountToCalibrate > rcPWM)PWMCountToCalibrate = rcPWM;
      setCoilPWM(PWMCountToCalibrate/(float)rcPWM);
      prevCalibrateSample = micros();
      prevExecute = millis();
    }
  }
}

void executeModeIdle(){
  readHallVal();
  //change to PID control if in the init interval 
  if ((currHallVal < INIT_CONTROL_INTERVAL_MAX) && (currHallVal > INIT_CONTROL_INTERVAL_MIN)){
    setMode(MODE_PID_CONTROL);
  }
}

void executeModePIDControl(){
  readHallVal();

  //if too far away then put the system back into idle and wait for the magnet to
  //be place back in range
  if (currHallVal < MIN_CONTROL_HALL_READING){
    debugPrint(" currPWM: " +String(currPWM) + "    currHall: " +String(currHallVal));
    setMode(MODE_IDLE);
   return; 
  }
  //if system is too close turn it off 
  if (currHallVal > MAX_CONTROL_HALL_READING){
   setMode(MODE_OFF);
   return; 
  } 
  
  unsigned long currTime = millis();
 
  //update the PWM at the CONTROL_UPDATE_FREQ
  if( currTime > (prevExecute + CONTROL_UPDATE_FREQ)){
     prevExecute = currTime;
     
     //P is the current error of the current position vs where we want the magnet to be 
     P = currSetPoint - currHallVal; 
     
     //I is a cumelative error, make sure it doesnt get too high. 
     I += P;
     I = constrain(I, -MAX_I, MAX_I); 
     
     //D is the derivative term ie the difference between the current position and the previous one 
     D = prevHallVal - currHallVal;
     
     float newPWM = currPWMMidpoint + (KP*P) + (KI*I) + (KD*D);
     debugPrint(" newPWM: " +String(newPWM) + "    currPWM: " +String(currPWM)+ "    currHall: " +String(currHallVal));
     //make sure we have a valid PWM value 
     newPWM = constrain( newPWM, 0, MAX_PWM);
     //set system PWM to new value 
     setCoilPWM(newPWM);
     graphPrint(String(10000*newPWM)+","+String(10000*(KP*P))+","+String(10000*(KI*I))+","+String(10000*(KD*D)));
     
//    Serial.println("currPWM:  " +String(currPWM) + "   SetPoint: "+ String(currSetPoint)+ 
//        "   Hall: "+ String(currHallVal));
//    Serial.println(" P: " +String(P) + "    I: " +String(I) + "    D: " +String(D));
//    Serial.println("KP: " +String(KP) +"   KI: " +String(KI) +"   KD: " +String(KD));
//    Serial.println("");
   }
}


void setMode(int mode){
    switch(mode)
    {
      case MODE_OFF:
          statusPrint("set mode OFF");
          currMode = MODE_OFF;
          digitalWrite(PIN_STATE_MODE_OFF,HIGH);
          digitalWrite(PIN_STATE_MODE_IDLE,LOW);
          digitalWrite(PIN_STATE_MODE_PID_CONTROL,LOW);
          break;
      case MODE_CALIBRATE:
          statusPrint("set mode CALIBRATE");
          currMode = MODE_CALIBRATE;
          PWMCountToCalibrate = 0;
          prevExecute = millis();
          prevCalibrateSample = micros();
          numCalibrateSample = 0;
          calibrateSampleSum = 0; 
          digitalWrite(PIN_STATE_MODE_OFF,HIGH);
          digitalWrite(PIN_STATE_MODE_IDLE,HIGH);
          digitalWrite(PIN_STATE_MODE_PID_CONTROL,HIGH);
          break; 
      case MODE_IDLE:
          statusPrint("set mode IDLE");
          resetToIdle();
          digitalWrite(PIN_STATE_MODE_OFF,LOW);
          digitalWrite(PIN_STATE_MODE_IDLE,HIGH);
          digitalWrite(PIN_STATE_MODE_PID_CONTROL,LOW);
          break;
      case MODE_PID_CONTROL:
          statusPrint("set mode CONTROL");
          currMode = MODE_PID_CONTROL;
          digitalWrite(PIN_STATE_MODE_OFF,LOW);
          digitalWrite(PIN_STATE_MODE_IDLE,LOW);
          digitalWrite(PIN_STATE_MODE_PID_CONTROL,HIGH);
          break; 
      default:
          break;
    }
}


void resetToIdle(){
  //current PID control values 
  P = 0;
  I = 0;
  D = 0;
  
  currMode = MODE_IDLE;
  setCoilPWM(IDLE_PWM);
  currPWMMidpoint = findBestCalibratePWM(INIT_PWM_MIDPOINT_FIELD_STR); 
  currSetPoint = INIT_SET_POINT;
  readHallVal();
  prevHallVal = currHallVal;
  prevExecute = millis();
}

//================================================================================
//                                I/O Management
//================================================================================

void processUserInput(){
  int input = Serial.read();
  switch(input){
    case 'p':
      KP -= ADJ_KP;
      break;
    case 'P':
      KP += ADJ_KP;
      break;
    case 'i':
      KI-= ADJ_KI;
      break;
    case 'I':
      KI += ADJ_KI;
      break;
    case 'd':
      KD -= ADJ_KD;
      break;
    case 'D':
      KD += ADJ_KD;
      break;
    case 's':
      currSetPoint -= 1;
      break;
    case 'S':
      currSetPoint += 1;
      break;
    case 'm':
      currPWMMidpoint -= 1;
      break;
    case 'M':
      currPWMMidpoint += 1;
      break;
    case 'o':
      printState();
      break;
    case 'O':
      autoPrint = !autoPrint;
      break;
    case 'c':
      setMode(MODE_CALIBRATE);
      break;
    default: 
         break;
  }
    
}

void readHallVal(){
  //save old hall value 
  prevHallVal = currHallVal;
  
  if (currMode == MODE_CALIBRATE){
    currHallVal = ((float) MMSum )/NUM_MM_VALS;
  }
  else{
    currHallVal = ((float) MMSum )/NUM_MM_VALS-calibrationVals[(int)((rcPWM-currPWMCounterVal)/CALIBRATE_PRECISION)];
  }
}

void debugPrint(String mesg) {
  Serial.println("$"+mesg);
}

void graphPrint(String mesg) {
  Serial.println("@"+mesg);
}

void statusPrint(String mesg) {
  Serial.println("#"+mesg);
}

void stdPrint(String mesg) {
  Serial.println(mesg);
}

//================================================================================
//                            Calculation Helper Methods
//================================================================================

//return the PWM best which will produce the given field (without a magnet), at the given calibration
float findBestCalibratePWM(float fieldStr){
  //assume 0 PWM has best error
  int closestPWM = 0; 
  float leastError = abs(calibrationVals[0] - fieldStr);
  //if any other PWM is better save that one 
  for( int i = 1; i <= rcPWM/CALIBRATE_PRECISION; i++){
    float thisError = abs(calibrationVals[i] - fieldStr); 
    if (thisError < leastError ){
      closestPWM = i;
      leastError = thisError; 
    }
  }
  statusPrint("closest calibration PWMCount found: "+String(closestPWM*CALIBRATE_PRECISION)+ "  (" +String(100*closestPWM*(float)CALIBRATE_PRECISION/(float)rcPWM)+"%)");
  return closestPWM*(float)CALIBRATE_PRECISION/(float)rcPWM; 
}

//sets the duty of the PWM to the decimal given
void setCoilPWM(float dutyPercent){
    currPWM = dutyPercent;
    currPWMCounterVal = rcPWM*(1.0-dutyPercent);
    TC_SetRA(pwmTc, pwmChannel, currPWMCounterVal); 
    //debugPrint("Set percent: "+ String(100*dutyPercent)
    TC_Start(pwmTc, pwmChannel);
}

//================================================================================
//                                 Setup and Interrupt Methods
//================================================================================

//sets up the interrupt for the PWM
void setupCoilPWM(Tc *tc, uint32_t channel){
  pwmTc = tc;
  pwmChannel = channel;
  
  pinMode(PIN_COIL,OUTPUT);
  pmc_set_writeprotect(false);
  pmc_enable_periph_clk(ID_TC0);
  /*
  // TC_CMR_WAVE - wave mode (TIOA and TIOB) are set as output
  // TC_CMR_WAVSEL_UPDOWN_RC - updown mode with automatic trigger on RC
  // TC_CMR_TCCLKS_TIMER_CLOCK1 - use clock 1
  */
  TC_Configure(pwmTc, pwmChannel, TC_CMR_WAVE | TC_CMR_WAVSEL_UPDOWN_RC | TC_CMR_TCCLKS_TIMER_CLOCK1);
  TC_SetRC(pwmTc, pwmChannel, rcPWM);
  //Duty Cycle
  setCoilPWM(currPWM);
  // Set to trigger on A instead of C
  pwmTc->TC_CHANNEL[pwmChannel].TC_IER=TC_IER_CPAS;
  pwmTc->TC_CHANNEL[pwmChannel].TC_IDR=~TC_IER_CPAS;
  NVIC_EnableIRQ((IRQn_Type)ID_TC0);
}

// PWM signal Interrupt
//updates pin value to create pwm signal
void TC0_Handler() {
    //record the current time on the couter 
    uint32_t cvb4 = pwmTc->TC_CHANNEL[pwmChannel].TC_CV;
    //Acknolwedge the IRQ event, 
    //***NOTE MUST BE BETWEEN READINGS OF TC_CV in order to introduce delay so
    //it can be checked whether counter is counting up or down 
    TC_GetStatus(TC0, 0);
    //if counting down then enter LOW phase of PWM, if counting up then enter HIGH phase of PWM 
    if(cvb4 > pwmTc->TC_CHANNEL[pwmChannel].TC_CV) digitalWrite(PIN_COIL, LOW);
    else digitalWrite(PIN_COIL, HIGH);
}

//sets up the interrupt for the moveing mean 
void setupMMInterupt(Tc *tc, uint32_t channel){
  pmc_set_writeprotect(false);
  pmc_enable_periph_clk(ID_TC3);
  TC_Configure(tc, channel, TC_CMR_WAVE | TC_CMR_WAVSEL_UP_RC | TC_CMR_TCCLKS_TIMER_CLOCK1);
  rcMM = VARIANT_MCK/2/MM_FREQ;
  TC_SetRC(tc, channel, rcMM);
  TC_Start(tc, channel);
  tc->TC_CHANNEL[channel].TC_IER=TC_IER_CPCS;
  tc->TC_CHANNEL[channel].TC_IDR=~TC_IER_CPCS;
  NVIC_EnableIRQ((IRQn_Type)ID_TC3);
}

//Moveing Mean read Hall Interrupt
//keeps a moving mean going in the MMSum value is retruenved by read hall method
void TC3_Handler() {
  //Acknolwedge the IRQ event
  TC_GetStatus(TC1, 0);
  //get rid of old val
  MMSum -= MMVals[nextMMToRead];
  //record new val and add to sum 
  int reading = analogRead(PIN_HALL_SENSOR);
  MMVals[nextMMToRead] = reading;
  MMSum += reading;
  //update next MM tht should be read in 
  nextMMToRead++; 
  nextMMToRead = nextMMToRead % NUM_MM_VALS; 
}


// Loop in this method until Arduino
// begins to talk to processing code
void setupConnection() {
  if (!REQUIRE_SERIAL) {
    return;
  }
  
  while(Serial.available() <= 0) {
    Serial.println("H");
    delay(500);
  }
  pHandShake = true;
  
}

void setup(){
  Serial.begin(115200);
    
  // Don't continue until connection to processing code is established
  setupConnection();
  
  statusPrint("SETUP");
  
  pinMode(PIN_STATE_MODE_OFF, OUTPUT);
  pinMode(PIN_STATE_MODE_IDLE, OUTPUT);
  pinMode(PIN_STATE_MODE_PID_CONTROL, OUTPUT);
  
  lastPrint = millis();
  autoPrint = false; 
  
  //use a 12 bit ADC
  analogReadResolution(12);
  
  //setup system interrupts 
  setupCoilPWM(TC0, 0);
  setupMMInterupt(TC1,0);
  
  // set the PWM to zero and wait a bit to make sure Moveing mean array is populated
  setCoilPWM(0);
  delay(1); 
  setMode(MODE_CALIBRATE);

  //current PID control wieghts  (will be adjusted for tuning the PID controller)
  KP = INIT_KP;
  KI = INIT_KI;
  KD = INIT_KD;
}

void loop(){
     // User commands.
   if( Serial.available() ) {   
      processUserInput(); 
   }
  
  //auto print if set
  conditionalPrint();
  
  //execute proper mode
  switch( currMode )
  {
    case MODE_OFF:
        executeModeOff();
        break;
    case MODE_CALIBRATE:
        executeModeCalibrate();
        break; 
    case MODE_IDLE:
        executeModeIdle();
        break;
    case MODE_PID_CONTROL:
        executeModePIDControl();
        break; 
    default:
        break;
  }  
}


