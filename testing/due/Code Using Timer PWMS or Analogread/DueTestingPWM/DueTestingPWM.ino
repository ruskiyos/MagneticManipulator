
const int PIN_COIL = 13; 
const int PIN_HALL_SENSOR = A0;

const float INIT_PWM = 0.5; 
const int PWM_FREQ = 4000;

const int delayMiliSec = 1000;

String readString;

unsigned long lastPrint;

float currPWM;

// PWM toggle var
boolean l = LOW;

// Timer and Channel
Tc *pwmTc;
uint32_t pwmChannel;
uint32_t rc;


// IRQ Handler
void TC0_Handler() {
    // Acknolwedge IRQ event
//  TC_GetStatus(TC0, 0);
//  digitalWrite(PIN_COIL, l = !l);
    //record the current time on the couter 
    uint32_t cvb4 = pwmTc->TC_CHANNEL[pwmChannel].TC_CV;
    //Acknolwedge the IRQ event, 
    //***NOTE MUST BE BETWEEN READINGS OF TC_CV in order to introduce delay so
    //it can be checked whether counter is counting up or down 
    TC_GetStatus(TC0, 0);
    //if counting down then enter LOW phase of PWM, if counting up then enter HIGH phase of PWM 
    if(cvb4 > pwmTc->TC_CHANNEL[pwmChannel].TC_CV) digitalWrite(PIN_COIL, LOW);
    else digitalWrite(PIN_COIL, HIGH);
}

void setupCoilPWM(Tc *tc, uint32_t channel)
{  
  pwmTc = tc;
  pwmChannel = channel;
  pinMode(PIN_COIL,OUTPUT);
  //pinMode(PIN_COIL,OUTPUT);
  pmc_set_writeprotect(false);
  pmc_enable_periph_clk(ID_TC0);
  pmc_enable_interrupt(ID_TC0);
  /*
  // TC_CMR_WAVE - wave mode
  // TC_CMR_WAVSEL_UPDOWN_RC - updown mode with automatic trigger on RC
  // TC_CMR_TCCLKS_TIMER_CLOCK1 - use clock 1
  */
  TC_Configure(pwmTc, pwmChannel, TC_CMR_WAVE | TC_CMR_WAVSEL_UPDOWN_RC | TC_CMR_TCCLKS_TIMER_CLOCK1 | TC_CMR_ACPC_NONE | TC_CMR_ACPA_TOGGLE | TC_SR_MTIOA);
  rc = VARIANT_MCK/2/PWM_FREQ; //2 because we selected TIMER_CLOCK1 above
  Serial.println("Timer Initialized!");
  TC_SetRC(pwmTc, pwmChannel, rc);
  Serial.println("rc: "+String(rc));
  //Duty Cycle
  writeCoilPWM(INIT_PWM);
  // Set to trigger on A instead of C
  pwmTc->TC_CHANNEL[pwmChannel].TC_IER=TC_IER_CPAS;
  pwmTc->TC_CHANNEL[pwmChannel].TC_IDR=~TC_IDR_CPAS;
  NVIC_EnableIRQ((IRQn_Type)ID_TC0);

}

void writeCoilPWM(float dutyPercent)
{
  int cvb4 = pwmTc->TC_CHANNEL[pwmChannel].TC_CV;
  int cvA = pwmTc->TC_CHANNEL[pwmChannel].TC_CV;
  uint32_t counterVal = rc*(1.0-dutyPercent);
  /*
  if (dutyPercent <= 0.001 && false) {
    Serial.println("Less rc: "+ String(rc)+"  countval: "+ String(counterVal));
    TC_Stop(pwmTc, pwmChannel);
    l = LOW; 
    pwmTc->TC_CHANNEL[pwmChannel].TC_CV = 0; 
    digitalWrite(PIN_COIL, LOW);
  }
  else if (dutyPercent >= 0.999&& false
  ){
    Serial.println("More rc: "+ String(rc)+"  countval: "+ String(counterVal));
    TC_Stop(pwmTc, pwmChannel);
    l = LOW; 
    pwmTc->TC_CHANNEL[pwmChannel].TC_CV = 0; 
    digitalWrite(PIN_COIL, HIGH);
  }
  else{
  //Duty Cycle
//    pwmTc->TC_CHANNEL[pwmChannel].TC_CV = 00000;
//    TC_Stop(pwmTc, pwmChannel);
//    Serial.println(pwmTc->TC_CHANNEL[pwmChannel].TC_CV);
*/
    TC_SetRA(pwmTc, pwmChannel, counterVal); 
    Serial.println("rc: "+ String(rc)+"  newCV: "+ String(counterVal)+ "  currCV: "+ String(cvb4)+ "->"+ String(cvA)+ "->"+ String(pwmTc->TC_CHANNEL[pwmChannel].TC_CV)+"  dutyPercent  " + String(dutyPercent));
    TC_Start(pwmTc, pwmChannel);
    Serial.println("rc: "+ String(rc)+"  newCV: "+ String(counterVal)+ "  currCV: "+ String(cvb4)+ "->"+ String(cvA)+ "->"+ String(pwmTc->TC_CHANNEL[pwmChannel].TC_CV)+"  dutyPercent  " + String(dutyPercent));
    /*
  }
  */
}

void setup()
{
   // initialize serial communication at 9600 bits per second:
   Serial.begin(115200);

   setupCoilPWM(TC0, 0);
   
   writeCoilPWM(INIT_PWM);
   currPWM = INIT_PWM; 
   
   lastPrint = millis();
   
}

void loop(){
  /*
   digitalWrite(4,HIGH);
   digitalWrite(5,HIGH);
   digitalWrite(6,HIGH);
   */
    // User commands.
   if( Serial.available() ) 
   { 
       while (Serial.available()) {
          char c = Serial.read();  //gets one byte from serial buffer
          readString += c; //makes the string readString
          delay(2);  //slow looping to allow buffer to fill with next character
        } 
        
        if (readString.length() >0) {
          char carray[readString.length() + 1]; //determine size of the array
          readString.toCharArray(carray, sizeof(carray)); //put readStringinto an array
          currPWM = atof(carray);  //convert readString into a number
        }
        readString=""; //empty for next input
       Serial.println("set PWM: "+ String(currPWM));
       writeCoilPWM(currPWM);
   }  
//  compoundedSample += analogRead(A0);
//  numSample ++;
  unsigned long currTime = millis();
  if ((currTime - lastPrint) > delayMiliSec){
    lastPrint = currTime;
    // read the input on analog pin 0:
    //int sensorValue = analogRead(A0);
    // print out the value you read:
    Serial.println(String(currPWM) + "       " + String(analogRead(PIN_HALL_SENSOR)));
  }
  
}
