
const int PIN_COIL = 13; 
const int PIN_HALL_SENSOR = A0;

const float INIT_PWM = 0.5; 
const int PWM_FREQ = 10000;

const int MM_FREQ = 100000;

const int delayMiliSec = 100;

String readString;

unsigned long lastPrint;

long compoundedSample =0;
volatile int numSample = 0; 

float currPWM;

//the most recent Hall value read in, and the value before that. 
float currHallVal;
float prevHallVal;

//number of moveign mean values to record
const int NUM_MM_VALS = 10;
// time between MM Samples in ms 
const int MM_PERIOD = 10;  

// Timer and Channel
Tc *pwmTc;
Tc *mmTc;
uint32_t pwmChannel;
uint32_t rcPWM;
uint32_t rcMM;


//next moveing average val to be overwritten 
volatile int nextMMToRead = 0;
//all of the most recent moveing average vals 
volatile int MMVals[NUM_MM_VALS];
volatile int MMSum = 0;

// IRQ Handler
void TC0_Handler() {
  // Acknolwedge IRQ event
//  TC_GetStatus(TC0, 0);
//  digitalWrite(PIN_COIL, l = !l);

    //record the current time on the couter 
    uint32_t cvb4 = pwmTc->TC_CHANNEL[pwmChannel].TC_CV;
    //Acknolwedge the IRQ event, 
    //***NOTE MUST BE BETWEEN READINGS OF TC_CV in order to introduce delay so
    //it can be checked whether counter is counting up or down 
    TC_GetStatus(TC0, 0);
    //if counting down then enter LOW phase of PWM, if counting up then enter HIGH phase of PWM 
    if(cvb4 > pwmTc->TC_CHANNEL[pwmChannel].TC_CV) digitalWrite(PIN_COIL, LOW);
    else digitalWrite(PIN_COIL, HIGH);
}

void setupCoilPWM(Tc *tc, uint32_t channel)
{  
  pwmTc = tc;
  pwmChannel = channel;
  
  pinMode(PIN_COIL,OUTPUT);
  pmc_set_writeprotect(false);
  pmc_enable_periph_clk(ID_TC0);
  /*
  // TC_CMR_WAVE - wave mode (TIOA and TIOB) are set as output
  // TC_CMR_WAVSEL_UPDOWN_RC - updown mode with automatic trigger on RC
  // TC_CMR_TCCLKS_TIMER_CLOCK1 - use clock 1
  */
  TC_Configure(pwmTc, pwmChannel, TC_CMR_WAVE | TC_CMR_WAVSEL_UPDOWN_RC | TC_CMR_TCCLKS_TIMER_CLOCK1);
  rcPWM = VARIANT_MCK/2/2/PWM_FREQ; //2 because we selected TIMER_CLOCK1 above
  TC_SetRC(pwmTc, pwmChannel, rcPWM);
  //Duty Cycle
  writeCoilPWM(INIT_PWM);
  // Set to trigger on A instead of C
  pwmTc->TC_CHANNEL[pwmChannel].TC_IER=TC_IER_CPAS;
  pwmTc->TC_CHANNEL[pwmChannel].TC_IDR=~TC_IER_CPAS;
  NVIC_EnableIRQ((IRQn_Type)ID_TC0);
}

void TC3_Handler() {
  TC_GetStatus(TC1, 0);
  readHallInterupt();
  //Serial.println(micros());
}

void setupMMInterupt(Tc *tc, uint32_t channel){
  pmc_set_writeprotect(false);
  pmc_enable_periph_clk(ID_TC3);
  TC_Configure(tc, channel, TC_CMR_WAVE | TC_CMR_WAVSEL_UP_RC | TC_CMR_TCCLKS_TIMER_CLOCK1);
  rcMM = VARIANT_MCK/2/MM_FREQ;
  TC_SetRC(tc, channel, rcMM);
  TC_Start(tc, channel);
  tc->TC_CHANNEL[channel].TC_IER=TC_IER_CPCS;
  tc->TC_CHANNEL[channel].TC_IDR=~TC_IER_CPCS;
  NVIC_EnableIRQ((IRQn_Type)ID_TC3);
  // 10ms
}

void readHallInterupt()       // interrupt service routine 
{
  //get rid of old val
  MMSum -= MMVals[nextMMToRead];
  //record new val and add to sum 
  int reading = analogRead(PIN_HALL_SENSOR);
  MMVals[nextMMToRead] = reading;
  MMSum += reading;
  //update next MM tht should be read in 
  nextMMToRead++; 
  nextMMToRead = nextMMToRead % NUM_MM_VALS; 
}

void writeCoilPWM(float dutyPercent)
{
    uint32_t counterVal = rcPWM*(1.0-dutyPercent);
    TC_SetRA(pwmTc, pwmChannel, counterVal); 
    TC_Start(pwmTc, pwmChannel);
}

void setup()
{
   Serial.begin(115200);
   setupCoilPWM(TC0, 0);
   
   writeCoilPWM(INIT_PWM);
   currPWM = INIT_PWM;
   setupMMInterupt(TC1,0);
   analogReadResolution(12); 
   
   // initialize serial communication at 9600 bits per second:
   lastPrint = millis();
   digitalWrite(4,HIGH);
   digitalWrite(5,HIGH);
   digitalWrite(6,HIGH);
   
}

void loop(){
    //Serial.println(numSample);
    //numSample++;
    // User commands.
   if( Serial.available() ) 
   { 
       while (Serial.available()) {
          char c = Serial.read();  //gets one byte from serial buffer
          readString += c; //makes the string readString
          delay(2);  //slow looping to allow buffer to fill with next character
        } 
        
        if (readString.length() >0) {
          char carray[readString.length() + 1]; //determine size of the array
          readString.toCharArray(carray, sizeof(carray)); //put readStringinto an array
          currPWM = atof(carray);  //convert readString into a number
        }
        readString=""; //empty for next input
       Serial.println("set PWM: "+ String(currPWM));
       writeCoilPWM(currPWM);
   }   

  unsigned long currTime = millis();
  if ((currTime - lastPrint) > delayMiliSec){
    lastPrint = currTime;
    readHallVal();
    // print out the value you read:
    Serial.println(String(currPWM) + "       " + String(currHallVal));
  }
}

  void readHallVal(){
  //save old hall value 
  prevHallVal = currHallVal;
  currHallVal = ((float)MMSum)/NUM_MM_VALS;
//  for (int i=0; i< NUM_MM_VALS;i++){
//    Serial.print(String(MMVals[i])+ ",");
//  }
//  Serial.println(")");

  
}

