#include <DueTimer.h>
//NOTE USING FASTER BAUD RATE

const int PIN_COIL = 11; // Pins 3 and 11 are connected to Timer 2.
const int PIN_HALL_SENSOR = A0;
const int PIN_HALL_SENSOR2 = A3;

const int INIT_PWM = 0; 

const int delayMiliSec = 100;

String readString;

unsigned long lastPrint;

long compoundedSample =0;
volatile int numSample = 0; 

int currPWM;

//number of moveign mean values to record
const int NUM_MM_VALS = 10;
// time between MM Samples in ms 
const int MM_PERIOD = 10;  


//next moveing average val to be overwritten 
volatile int nextMMToRead = 0;
//all of the most recent moveing average vals 
volatile int MMVals[NUM_MM_VALS];
volatile int MMVals2[NUM_MM_VALS];
volatile int MMSum = 0;
volatile int MMSum2 = 0;



void setupCoilPWM()
{  
//   // Setup the timer 2 as Phase Correct PWM, 3921 Hz.
//   pinMode(3, OUTPUT);
//   pinMode(11, OUTPUT);
//   // Timer 2 register: WGM20 sets PWM phase correct mode, COM2x1 sets the PWM out to channels A and B.
//   TCCR2A = 0;
//   TCCR2A = _BV(COM2A1) | _BV(COM2B1) | _BV(WGM20);
//   // Set the prescaler to 8, the PWM freq is 16MHz/255/2/<prescaler> ie 3921.16Hz
//     TCCR2B = 0x02;
}

void setupMMInterupt(){
	Timer3.attachInterrupt(readHallInterupt);
	Timer3.start(10);
}

void readHallInterupt()       // interrupt service routine 
{
  //get rid of old val
  MMSum -= MMVals[nextMMToRead];
  MMSum2 -= MMVals2[nextMMToRead];
  
  //record new val and add to sum 
  int reading = analogRead(PIN_HALL_SENSOR);
  int reading2 = analogRead(PIN_HALL_SENSOR2);
  
  MMVals[nextMMToRead] = reading;
  MMSum += reading;
  
  MMVals2[nextMMToRead] = reading2;
  MMSum2 += reading2;
  
  //update next MM tht should be read in 
  nextMMToRead++; 
  
  //THIS IS A TEST PROGRAM UTILIZE READINGS AT MAX RATE
  if (nextMMToRead % NUM_MM_VALS == 0) readHallVal();
  
  nextMMToRead = nextMMToRead % NUM_MM_VALS; 
}

void writeCoilPWM(uint8_t pin, int val)
{
//  currPWM = val;
//  OCR2A = currPWM;
}

void setup()
{
   setupCoilPWM();
   
   writeCoilPWM(PIN_COIL, INIT_PWM);
   currPWM = INIT_PWM;
   setupMMInterupt();
   analogReadResolution(12); 
   
   // initialize serial communication at 9600 bits per second:
   Serial.begin(115200);
   
   lastPrint = millis();
   digitalWrite(4,HIGH);
   digitalWrite(5,HIGH);
   digitalWrite(6,HIGH);
   
}

void loop(){
//    //Serial.println(numSample);
//    //numSample++;
//    // User commands.
//   if( Serial.available() ) 
//   { 
//       while (Serial.available()) {
//          char c = Serial.read();  //gets one byte from serial buffer
//          readString += c; //makes the string readString
//          delay(2);  //slow looping to allow buffer to fill with next character
//        } 
//        
//        if (readString.length() >0) {
//          currPWM = readString.toInt();  //convert readString into a number
//        }
//        readString=""; //empty for next input
//         
//       writeCoilPWM(PIN_COIL, currPWM);
//       Serial.println("set PWM: "+ String(currPWM));
//   }  
//
//  unsigned long currTime = millis();
//  if ((currTime - lastPrint) > delayMiliSec){
//    lastPrint = currTime;
//    readHallVal();
//    // print out the value you read:
//    Serial.println(String(currPWM) + "       " + String(currHallVal));
//  }
}

  void readHallVal(){

  float currHallVal = ((float)MMSum)/NUM_MM_VALS;
  float currHallVal2 = ((float)MMSum2)/NUM_MM_VALS;
  
  //print out val for polling 
  Serial.println(String(currHallVal) + "    " + String(currHallVal2)+ "    " + String( currHallVal-currHallVal2));
  
}

