
#include "pwm01.h"
const int PIN_COIL = 9; 
const int PIN_HALL_SENSOR = A0;

const float INIT_PWM = 0.5;
const float ADJ_PWM = 0.001;
const int adjDelayMiliSec = 50;

const uint32_t PWM_FREQ = 10000;

const int MM_FREQ = 100000;

const int delayMiliSec = 1000;
const int MAX_PWM_COUNT = 65535; // 2^16 - 1

String readString;

unsigned long lastPrint;
unsigned long lastAdj;

long compoundedSample =0;
volatile int numSample = 0; 

float currPWM;

//the most recent Hall value read in, and the value before that. 
float currHallVal;
float prevHallVal;

//number of moveign mean values to record
const int NUM_MM_VALS = 10;
// time between MM Samples in ms 
const int MM_PERIOD = 10;  

// Timer and Channel
Tc *pwmTc;
Tc *mmTc;
uint32_t pwmChannel;
uint32_t rcPWM;
uint32_t rcMM;


//next moveing average val to be overwritten 
volatile int nextMMToRead = 0;
//all of the most recent moveing average vals 
volatile int MMVals[NUM_MM_VALS];
volatile int MMSum = 0;

boolean incrementPWM = false; 

void setupCoilPWM(Tc *tc, uint32_t channel)
{  
    // Set PWM Resolution
    pwm_set_resolution(16);  

    pwm_setup( PIN_COIL, PWM_FREQ, 1);  // Pin 9 freq set to "pwm_freq2" on clock B
      
    writeCoilPWM(INIT_PWM);
}

void TC3_Handler() {
  TC_GetStatus(TC1, 0);
    //get rid of old val
  MMSum -= MMVals[nextMMToRead];
  //record new val and add to sum 
  int reading = analogRead(PIN_HALL_SENSOR);
  MMVals[nextMMToRead] = reading;
  MMSum += reading;
  //update next MM tht should be read in 
  nextMMToRead++; 
  nextMMToRead = nextMMToRead % NUM_MM_VALS; 
  //Serial.println(micros());
}

void setupMMInterupt(Tc *tc, uint32_t channel){
  pmc_set_writeprotect(false);
  pmc_enable_periph_clk(ID_TC3);
  TC_Configure(tc, channel, TC_CMR_WAVE | TC_CMR_WAVSEL_UP_RC | TC_CMR_TCCLKS_TIMER_CLOCK1);
  rcMM = VARIANT_MCK/2/MM_FREQ;
  TC_SetRC(tc, channel, rcMM);
  TC_Start(tc, channel);
  tc->TC_CHANNEL[channel].TC_IER=TC_IER_CPCS;
  tc->TC_CHANNEL[channel].TC_IDR=~TC_IER_CPCS;
  NVIC_EnableIRQ((IRQn_Type)ID_TC3);
  // 10ms
}

void writeCoilPWM(float dutyPercent)
{
    if (dutyPercent < 0 ){
      pwm_write_duty( PIN_COIL, ((uint32_t)0));
    }
    else if ( dutyPercent > 1){
      pwm_write_duty( PIN_COIL, ((uint32_t)MAX_PWM_COUNT));
    }
    else{
      pwm_write_duty( PIN_COIL, (uint32_t)(MAX_PWM_COUNT*dutyPercent));
    }
}

void setup()
{
   Serial.begin(115200);
   setupCoilPWM(TC0, 0);
   
   writeCoilPWM(INIT_PWM);
   currPWM = INIT_PWM;
    analogReadResolution(12);
   setupMMInterupt(TC1,0);
 
   
   // initialize serial communication at 9600 bits per second:
   lastPrint = millis();
   digitalWrite(4,HIGH);
   digitalWrite(5,HIGH);
   digitalWrite(6,HIGH);
   
}

void loop(){
    //Serial.println(numSample);
    //numSample++;
    // User commands.
   if( Serial.available() ) 
   { 
       while (Serial.available()) {
          char c = Serial.read();  //gets one byte from serial buffer
          if (c = 'i'){
            incrementPWM = !incrementPWM;
            Serial.println("set increment: "+ String(incrementPWM)); 
            return; 
          } 
          readString += c; //makes the string readString
          delay(2);  //slow looping to allow buffer to fill with next character
        } 
        
        if (readString.length() >0) {
          char carray[readString.length() + 1]; //determine size of the array
          readString.toCharArray(carray, sizeof(carray)); //put readStringinto an array
          currPWM = atof(carray);  //convert readString into a number
        }
        readString=""; //empty for next input
       Serial.println("set PWM: "+ String(currPWM));
       writeCoilPWM(currPWM);
   }   

  unsigned long currTime = millis();
  if ((currTime - lastPrint) > delayMiliSec){
    lastPrint = currTime;
    readHallVal();
    // print out the value you read:
    Serial.println(String(currPWM) + "       " + String(currHallVal));
  }
  
  if (incrementPWM && (currTime - lastAdj) > adjDelayMiliSec){
    lastAdj = currTime;
    currPWM = (currPWM + ADJ_PWM);
    if (currPWM >1) currPWM = 0;
    writeCoilPWM(currPWM);
  }
}

  void readHallVal(){
  //save old hall value 
  prevHallVal = currHallVal;
  currHallVal = ((float)MMSum)/NUM_MM_VALS;
//  for (int i=0; i< NUM_MM_VALS;i++){
//    Serial.print(String(MMVals[i])+ ",");
//  }
//  Serial.println(")");

  
}

