volatile boolean l;

/*
//TC1 ch 0
void TC3_Handler(){
  TC_GetStatus(TC1, 0);
  digitalWrite(13, l = !l);
}

void startTimer(Tc *tc, uint32_t channel, IRQn_Type irq, uint32_t frequency) {
        pmc_set_writeprotect(false);
        pmc_enable_periph_clk((uint32_t)irq);
        TC_Configure(tc, channel, TC_CMR_WAVE | TC_CMR_WAVSEL_UPDOWN_RC | TC_CMR_TCCLKS_TIMER_CLOCK4);
        uint32_t rc = VARIANT_MCK/128/frequency; //128 because we selected TIMER_CLOCK4 above
        //TODO Duty Cycle
        TC_SetRA(tc, channel, rc/2); //50% high, 50% low
        TC_SetRC(tc, channel, rc);
        TC_Start(tc, channel);
        tc->TC_CHANNEL[channel].TC_IER=TC_IER_CPCS;
        tc->TC_CHANNEL[channel].TC_IDR=~TC_IER_CPCS;
        NVIC_EnableIRQ(irq);
}
*/

// IRQ Handler
void TC0_Handler() {
  TC_GetStatus(TC0, 0);
  digitalWrite(13, l = !l);
}

void setupTimer(Tc *tc, uint32_t channel) {
        uint32_t frequency = 4000;
        pmc_set_writeprotect(false);
        pmc_enable_periph_clk(ID_TC0);
        // TC_CMR_WAVE - wave mode
        // TC_CMR_WAVSEL_UPDOWN_RC - updown mode with automatic trigger on RC
        // TC_CMR_TCCLKS_TIMER_CLOCK1 - use clock 1
        TC_Configure(tc, channel, TC_CMR_WAVE | TC_CMR_WAVSEL_UPDOWN_RC | TC_CMR_TCCLKS_TIMER_CLOCK1);
        uint32_t rc = VARIANT_MCK/2/frequency; //2 because we selected TIMER_CLOCK1 above
        //Duty Cycle
        TC_SetRA(tc, channel, rc/4); //25% high, 75% low
        TC_SetRC(tc, channel, rc);
        TC_Start(tc, channel);
        // Set to trigger on A instead of C
        tc->TC_CHANNEL[channel].TC_IER=TC_IER_CPAS;
        tc->TC_CHANNEL[channel].TC_IDR=~TC_IER_CPAS;
        NVIC_EnableIRQ((IRQn_Type)ID_TC0);
}

void setup(){
        pinMode(13,OUTPUT);
        //TC1 channel 0, the IRQ for that channel and the desired frequency
        //startTimer(TC1, 0, TC3_IRQn, 4); 
        
        setupTimer(TC0, 0);
}

void loop(){

}

