// File: MagneticManipulatorDue
// Authors:
//    Marley Rutkowski
//    John Olennikov

#include "pwm01.h" // library used for createing PWM signal 

//================================================================================
//                                Program Constants
//================================================================================
// Enable to talk to Processing Code
const boolean REQUIRE_SERIAL = true;
const boolean GRAPH_PRINT = true;
const boolean DEBUG_PRINT = true;

// Handshake delay
const int HANDSHAKE_DELAY = 50;

//system I/O pin definititions
const int PIN_COIL = 9; //note must be pin 6-9

//pins of hall signals the readings for these are set up in ADC_setup, 
//since hardware triggered ADCs are used these pins are hardcoded 
//PIN_ISOLATED_OBJECT_SIGNAL = A0-A7
//PIN_COIL_STRENGTH_SIGNAL = A8-A11

const int PIN_ENABLE = 52;
const int PIN_REQUIRE_SERIAL = 50;
//current mode indicator lights red, yellow, and green respectively
const int PIN_STATE_MODE_OFF = 53;
const int PIN_STATE_MODE_IDLE = 51;
const int PIN_STATE_MODE_PID_CONTROL = 49;

//time to wait for a handshake from gui 
const int HANDSHAKE_WAIT_TIME = 2000000;
//time between bewteen exectutions (microSec)
//of a check and update of the current system state 
const int CONTROL_UPDATE_WAIT_TIME = 1000;
//of incrmenting movement
const int MOVEMENT_WAIT_TIME = 150000;
//of auto print 
const int AUTO_PRINT_WAIT_TIME = 1000000;
//the number of control executions per graph print 
const int EXECUTIONS_PER_PRINT = 25;
//the number of calibration values per print 
const int CALIBRATIONS_PER_PRINT = 50;
//of reading calibration value after setting PWM
const int CALIBRATE_WAIT_TIME = 1000;
//of takeing samples for given calibrate
//note should not be less than ((1/mm_FREQ)* NUM_MM_VALS)micros or vals may get used twice
const int CALIBRATE_SAMPLE_WAIT_TIME = 100;

//Frequency of the PWM pulse
const uint32_t PWM_FREQ = 10000;
// 2^16 - 1 because use 16 bit register value
const uint32_t MAX_PWM_COUNT = 65535; 
//increments of PWM count to measure at during calibration
const int CALIBRATE_PRECISION = 15;

//Frequency at which moveing mean values are read in (ADCs are triggered)
const int MM_FREQ = 100000;
//number of moveign mean values to record
const int NUM_MM_VALS = 10;
//number of revious values to record
//derivatives will be taken over time CONTROL_UPDATE_WAIT_TIME*NUM_PREV_VALS
const int NUM_PREV_VALS = 100;

// Minimum magnetic field when in idle mode (needed to properly orientate the magnet).
const float IDLE_PWM = .08;
const int MAX_PWM = 1;

//The field strength (as read in on the hall sensor w/o magnet present) critical point where a
//magnet is lifted at the init setpoint. Used for calibration and determineing init PWMMidpoint
const float INIT_PWM_MIDPOINT_FIELD_STR = 260;
const float INIT_SET_POINT = 678;

//system will change state from IDLE to PID_CONTROL when the magnet enters this interval
const int INIT_CONTROL_INTERVAL_MAX = 670;
const int INIT_CONTROL_INTERVAL_MIN = 580;

//The range that the system will countinue to operate in control mode for
const int MIN_CONTROL_HALL_READING = 55;
const int MAX_CONTROL_HALL_READING = 3500;

//if turned off will resume control when brought within tolerance of currSetPoint
const int RESUME_CONTROL_TOLERANCE = 700;

//the maximume cumilative error the sytem should allow in its calculations
const int MAX_I = 20000;

// State machine constants.
const byte MODE_OFF = 0;
const byte MODE_CALIBRATE = 1;
const byte MODE_IDLE = 2;
const byte MODE_PID_CONTROL = 3;

//PID control wieghts
const float INIT_KP = 0.000427;
const float INIT_KI = 0.000004;
const float INIT_KD = 0.006000;

//PID control wieght adjustment amounts used for tuning PID control algorythm
const float ADJ_KP = .00001;
const float ADJ_KI = .000001;
const float ADJ_KD = .0001;
const float ADJ_MID = 10;
const float ADJ_SET = 5;
const float ADJ_HEIGTH = 5;

//how much the square root of the setpoint is incremented per movement execution
const float MOVEMENT_INCREMENT =  .15;

//system control paramenters for effective levitation at a range of setpoints 
//setPoint,MidPointFieldStr,KP,KI,KD
const float movementControlSets[][5] {
//  {1713,100,0.000277,0.000004,0.003000},
//  {1603,110,0.000277,0.000004,0.003100},
  {1448,120,0.000217,0.000004,0.003200},
  {1293,140,0.000217,0.000004,0.003300},
  {1153,160,0.000247,0.000004,0.003400},
  {1053,180,0.000257,0.000004,0.003700},
  {1003,190,0.000257,0.000004,0.003800},
  {923,200,0.000317,0.000004,0.004000},
  {873,210,0.000337,0.000004,0.004400},
  {778,230,0.000367,0.000004,0.005500},
  {678,260,0.000427,0.000004,0.006800}, //default
  {583,290,0.0009,0.000004,0.007900},
  {553,310,0.000607,0.000004,0.008300},
  {463,360,0.001007,0.000004,0.012400},
  {383,420,0.001317,0.000004,0.014800},
  {323,480,0.001677,0.000006,0.020000},
  {263,580,0.002077,0.000002,0.036200},
  {223,670,0.003467,0.000002,0.043700}
//{183,840,0.007027,0.0000005,0.075900}
};

const float objectControlSet[][5] {
  {678,260,0.000427,0.000004,0.006800}, //default
  {588,600,0.001407,0.000004,0.018300}, //globe
  {613,780,0.001267,0.000004,0.019600}, //Smiley
  {478,750,0.001717,0.000004,0.020300} //Nux
 
};

//    
//================================================================================
//                              Variable Declarations
//================================================================================
boolean enabled = false; 
// Serial connection handshake status
boolean pHandShake = false;
//whether to autoprint system info a regular rate
boolean autoPrint;

//the current state machine mode
int currMode;

//the PWM that is currently being applied
float currPWM;
uint32_t currPWMCounterVal;
float currPWMMidpoint;
float currPWMMidpointFieldStr;

float currSetPoint;
float maxMinSetPoint[2];
int maxCtrlSetIndex;

boolean moveUpDown[2] = {false,false}; 

//the most recent Hall value read in, and the value before that.
float currHallVal;
float currRawHallVal;
float prevHallVal;
float prevHallVals[NUM_PREV_VALS];
int oldestPrevVal = 0; 

//array of Hall values associate with given PWMs
float calibrationVals[(int)(MAX_PWM_COUNT / CALIBRATE_PRECISION) + 1];
int PWMCountToCalibrate;
//time of prev calibration sample (micros)
unsigned long prevCalibrateSample;
unsigned int numCalibrateSample;
float calibrateSampleSum;

//second mode indicator used to make the mm interrupt routine more efficient
boolean readFromCoilHall; 
//next moveing average val to be overwritten
volatile int nextMMToRead = 0;
//all of the most recent moveing average vals
volatile int MMVals[NUM_MM_VALS];
volatile int MMSum = 0;

//last time a pertinent function was executed, what this function was depends on mode
unsigned long prevExecute;
unsigned long lastPrint;
unsigned long prevMove;
//number of update executions since last print 
unsigned int printCounter;

//current PID control wieghts  (will be adjusted for tuning the PID controller)
float KP;
float KI;
float KD;
//current PID control values
float P;
float I;
float D;


//================================================================================
//                                  Execution Modes
//================================================================================

void executeModeOff() {
  setCoilPWM(0);
  readHallVal();

  //if we are back in a resonable range go back to idle
  if (currHallVal <= (RESUME_CONTROL_TOLERANCE + currSetPoint)) {
    setMode(MODE_PID_CONTROL);
    //reset PID params
    P = 0;
    I = 0;
    D = 0;
  }
}

void executeModeCalibrate() {

  //if enough time has passed take another sample
  if  (micros() > (prevCalibrateSample + CALIBRATE_SAMPLE_WAIT_TIME)) {
    readHallVal();
    //debugPrint(" currPWM: " +String(currPWM) + "    currHall: " +String(currHallVal));
    prevCalibrateSample = micros();
    numCalibrateSample += 1;
    calibrateSampleSum += currHallVal;
  }

  //update the PWM at the CONTROL_UPDATE_WAIT_TIME
  if ( micros() > (prevExecute + CALIBRATE_WAIT_TIME)) {
    // if new calibration then init calibration vals to 0
    if (PWMCountToCalibrate == 0) {
      for (int i = 0; i <= (int)(MAX_PWM_COUNT / CALIBRATE_PRECISION); i++)  calibrationVals[i] = 0;
    }

    calibrationVals[(int)(PWMCountToCalibrate / CALIBRATE_PRECISION)] = calibrateSampleSum / numCalibrateSample;
    calibratePrint("Samples: " + String(numCalibrateSample) + " PWMCount: " + String(PWMCountToCalibrate) + "/" + String(MAX_PWM_COUNT) + " (" +
                   String(100 * (float)PWMCountToCalibrate / (float)MAX_PWM_COUNT) + "%) Hall: " + String(calibrationVals[(int)(PWMCountToCalibrate / CALIBRATE_PRECISION)]));
    numCalibrateSample = 0;
    calibrateSampleSum = 0;
    //if our PWMCountToCalibrate is greater than our MAX_PWM then we are done calibrateing
    if (PWMCountToCalibrate >= MAX_PWM_COUNT) {
      statusPrint("Done CALIBRATE");
      setMode(MODE_IDLE);
    }
    else {
      //set up read of next value
      PWMCountToCalibrate += CALIBRATE_PRECISION;
      if (PWMCountToCalibrate > MAX_PWM_COUNT)PWMCountToCalibrate = MAX_PWM_COUNT;
      setCoilPWM(PWMCountToCalibrate / (float)MAX_PWM_COUNT);
      prevCalibrateSample = micros();
      prevExecute = micros();
    }
  }
}

void executeModeIdle() {
  readHallVal();
  //change to PID control if in the init interval
  if ((currHallVal < INIT_CONTROL_INTERVAL_MAX) && (currHallVal > INIT_CONTROL_INTERVAL_MIN)) {
    setMode(MODE_PID_CONTROL);
  }
  
    unsigned long currTime = micros();
  //graphPrint at the 1/CONTROL_UPDATE_WAIT_TIME frequency 
  if ( currTime > (prevExecute + CONTROL_UPDATE_WAIT_TIME)) {
    prevExecute = currTime;
    graphPrint(); 
  }
}

void executeModePIDControl() {
  
  readHallVal();

  //if too far away then put the system back into idle and wait for the magnet to
  //be place back in range
  if (currHallVal < MIN_CONTROL_HALL_READING) {
    debugPrint(" currPWM: " + String(currPWM) + "    currHall: " + String(currHallVal));
    setMode(MODE_IDLE);
    return;
  }
  //if system is too close turn it off
  else if (currHallVal > MAX_CONTROL_HALL_READING) {
    setMode(MODE_OFF);
    return;
  }

  unsigned long currTime = micros();

  //update the PWM at the CONTROL_UPDATE_WAIT_TIME
  if ( currTime > (prevExecute + CONTROL_UPDATE_WAIT_TIME)) {

    //P is the current error of the current position vs where we want the magnet to be
    P = currSetPoint - currHallVal;

    //I is a cumelative error, make sure it doesnt get too high.
    I += P;
    I = constrain(I, -MAX_I, MAX_I);

    //D is the derivative term ie the difference between the current position and the previous one,
    //use the oldest previous value so that the derivativwe is taken over enough time
    D = prevHallVals[oldestPrevVal] - currHallVal;

    float newPWM = currPWMMidpoint + (KP * P) + (KI * I) + (KD * D);
    
    //make sure we have a valid PWM value
    newPWM = constrain( newPWM, 0, MAX_PWM);
    //set system PWM to new value
    setCoilPWM(newPWM);
    
    //print current values to gui graph
    graphPrint(); 
    
    //update time 
    prevExecute = currTime;
  }
}

//sets the curret mode to the given value updateing state LEDs
void setMode(int mode) {
  //by defualt read hall signal that has been processed to only contain information on object position 
  readFromCoilHall = false; 
  switch (mode)
  {
    case MODE_OFF:
      statusPrint("set mode OFF");
      currMode = MODE_OFF;
      digitalWrite(PIN_STATE_MODE_OFF, HIGH);
      digitalWrite(PIN_STATE_MODE_IDLE, LOW);
      digitalWrite(PIN_STATE_MODE_PID_CONTROL, LOW);
      break;
    case MODE_CALIBRATE:
      statusPrint("set mode CALIBRATE");
      currMode = MODE_CALIBRATE;
      PWMCountToCalibrate = 0;
      prevExecute = micros();
      prevCalibrateSample = micros();
      numCalibrateSample = 0;
      calibrateSampleSum = 0;
      readFromCoilHall = true;
      digitalWrite(PIN_STATE_MODE_OFF, HIGH);
      digitalWrite(PIN_STATE_MODE_IDLE, HIGH);
      digitalWrite(PIN_STATE_MODE_PID_CONTROL, HIGH);
      break;
    case MODE_IDLE:
      statusPrint("set mode IDLE");
      resetToIdle();
      digitalWrite(PIN_STATE_MODE_OFF, LOW);
      digitalWrite(PIN_STATE_MODE_IDLE, HIGH);
      digitalWrite(PIN_STATE_MODE_PID_CONTROL, LOW);
      break;
    case MODE_PID_CONTROL:
      statusPrint("set mode CONTROL");
      currMode = MODE_PID_CONTROL;
      digitalWrite(PIN_STATE_MODE_OFF, LOW);
      digitalWrite(PIN_STATE_MODE_IDLE, LOW);
      digitalWrite(PIN_STATE_MODE_PID_CONTROL, HIGH);
      break;
    default:
      break;
  }
}

void resetToIdle() {
  //current PID control values
  P = 0;
  I = 0;
  D = 0;

  currMode = MODE_IDLE;
  setCoilPWM(IDLE_PWM);
  currPWMMidpoint = findBestCalibratePWM(currPWMMidpointFieldStr);
  //read twice to flush out any bad prev hall vals
  readHallVal();
  readHallVal();
  prevExecute = micros();
}

//================================================================================
//                                I/O Management
//================================================================================

void processUserInput() {
  int input = Serial.read();
  switch (input) {
    case 'p':
      KP -= ADJ_KP;
      break;
    case 'P':
      KP += ADJ_KP;
      break;
    case 'i':
      KI -= ADJ_KI;
      break;
    case 'I':
      KI += ADJ_KI;
      break;
    case 'd':
      KD -= ADJ_KD;
      break;
    case 'D':
      KD += ADJ_KD;
      break;
    case 's':
      currSetPoint -= ADJ_SET;
      break;
    case 'S':
      currSetPoint += ADJ_SET;
      break;
    case 'm':
      currPWMMidpointFieldStr -= ADJ_MID;
      currPWMMidpoint = findBestCalibratePWM(currPWMMidpointFieldStr);
      break;
    case 'M':
      currPWMMidpointFieldStr += ADJ_MID;
      currPWMMidpoint = findBestCalibratePWM(currPWMMidpointFieldStr);
      break;
    case 'u':
      setControlVals(currSetPoint - ADJ_HEIGTH);
      break;
    case 'U':
      setControlVals(currSetPoint + ADJ_HEIGTH);
      break;
    case 'o':
      printState();
      break;
    case 'O':
      autoPrint = !autoPrint;
      break;
    case 'c':
      setMode(MODE_CALIBRATE);
      break;
    case '+':
      moveUpDown[1] = true;
      debugPrint("setMove Up"); 
      break;
    case '-':
      moveUpDown[1] = false;
      debugPrint("setMove Down");
      break;
    case '=':
      moveUpDown[0] = !moveUpDown[0];
      if (moveUpDown[0]) debugPrint("enable move");
      else debugPrint("disable move");
      break;
    case '0':
      loadControlSet(0);
      break;
    case '1':
      loadControlSet(1);
      break;
    case '2':
      loadControlSet(2);
      break;
    case '3':
      loadControlSet(3);
      break;
    default:
      break;
  }
}

//update the hall values by takeing an average of tyhe running sum 
void readHallVal() {
  //save old hall value
  prevHallVal = currHallVal;
  
  prevHallVals[oldestPrevVal] = currHallVal;
  //updatePrevToUse so that the oldest value is used 
  oldestPrevVal = (oldestPrevVal+1) % NUM_PREV_VALS;
  
  //read new value by avarageing the current moveing sum 
  currHallVal = ((float) MMSum ) / NUM_MM_VALS;
}

//================================================================================
//                                 Print to Serial
//================================================================================

void calibratePrint(String mesg) {
     //dont actually print everytime graphPrint is called becasue serial communication is too CPU intensive, print every CALIBRATIONS_PER_PRINT exectutions
    printCounter++;
    if (printCounter >= CALIBRATIONS_PER_PRINT){
      printCounter = 0; 
      statusPrint(mesg);
    }
}

void conditionalPrint() {
  unsigned long currTime = micros();
  if (autoPrint && (currTime - lastPrint) > AUTO_PRINT_WAIT_TIME) {
    lastPrint = currTime;
    printState();
  }
}
void debugPrint(String mesg) {
  if (DEBUG_PRINT) Serial.println("$" + mesg);
}

void endMovePrint(String mesg) {
  Serial.println("!" +mesg);
}

void graphPrint() {
  if (GRAPH_PRINT){
    //dont actually print everytime graphPrint is called becasue serial communication is too CPU intensive, print every EXECUTIONS_PER_PRINT exectutions
    printCounter++;
    if (printCounter >= EXECUTIONS_PER_PRINT){
      printCounter = 0; 
      Serial.println("@" + String(currSetPoint) + "," + String(currHallVal)+ "|"
          + String(10000 * currPWM) + "|" 
          + String(10000 * (KP * P)) + "," + String(10000 * (KI * I)) + "," + String(10000 * (KD * D))
      );
    }
  }
}

String modeToSting() {
  switch ( currMode )
  {
    case MODE_OFF:
      executeModeOff();
      return "OFF";
    case MODE_CALIBRATE:
      return "CALIBRATE";
    case MODE_IDLE:
      return "IDLE";
    case MODE_PID_CONTROL:
      return "CONTROL";
    default:
      break;
  }
}

void printState() {
  // Print out current values
  statusPrint(modeToSting());
  statusPrint("currPWM:  " + String(currPWM * 1000) + " SetPoint: " + String(currSetPoint) +
              "   PWMMid " + String(currPWMMidpoint * 1000) + "  MidFieldStr " + String(currPWMMidpointFieldStr) + "   Hall: " + String(currHallVal));
  statusPrint(" P: " + String(P * 1000) + "%    I: " + String(I * 1000) + "%    D: " + String(D * 1000) + "%");
  statusPrint("KP: " + String(KP * 10000) + "%   KI: " + String(KI * 10000) + "%   KD: " + String(KD * 10000) + "%");
}

void statusPrint(String mesg) {
  Serial.println("#" + mesg);
}

void stdPrint(String mesg) {
  Serial.println(mesg);
}

//================================================================================
//                            Calculation Helper Methods
//================================================================================

//return the PWM best which will produce the given field (without a magnet), at the given calibration
float findBestCalibratePWM(float fieldStr) {
  //assume 0 PWM has best error
  int closestPWM = 0;
  float leastError = abs(calibrationVals[0] - fieldStr);
  //if any other PWM is better save that one
  for ( int i = 1; i <= MAX_PWM_COUNT / CALIBRATE_PRECISION; i++) {
    float thisError = abs(calibrationVals[i] - fieldStr);
    if (thisError < leastError ) {
      closestPWM = i;
      leastError = thisError;
    }
  }
  return closestPWM * (float)CALIBRATE_PRECISION / (float)MAX_PWM_COUNT;
}

//increments levitation point up  or down linearly
void executeMove(){
  //if move is enabled
  if (moveUpDown[0]){
    //only execute if enough time has passed
    unsigned long currTime = micros();
    if ( currTime > (prevMove + MOVEMENT_WAIT_TIME)){
        float newSetPoint; 
        //whether moveing up or down, linearize movement by increaseing the square root of the setpoint
        if(moveUpDown[1]){
          newSetPoint = sq(sqrt(currSetPoint)+MOVEMENT_INCREMENT);
        }
        else {
          newSetPoint = sq(sqrt(currSetPoint)-MOVEMENT_INCREMENT);
        }
        //make sure new set point is valid
        if (newSetPoint < movementControlSets[maxCtrlSetIndex][0] || newSetPoint > movementControlSets[0][0]){
          moveUpDown[0] = false;
          endMovePrint("Max range reached terminateing move");
        }
      newSetPoint = constrain(newSetPoint, movementControlSets[maxCtrlSetIndex][0], movementControlSets[0][0]);
      //update all of the control values based on new setpoint
      setControlVals(newSetPoint);
      prevMove = currTime;
    }
  } 
}

void loadControlSet(int toLoad){
  if (toLoad >= sizeof(objectControlSet)/ sizeof(objectControlSet[0])){
    debugPrint("Load control set failed, invalid control set " + String(toLoad)); 
    return; 
  }
  
  debugPrint("Load control set " + String(toLoad));
  currSetPoint = objectControlSet[toLoad][0];
  currPWMMidpointFieldStr = objectControlSet[toLoad][1];
  currPWMMidpoint = findBestCalibratePWM(currPWMMidpointFieldStr);
  
  KP =  objectControlSet[toLoad][2];;
  KI =  objectControlSet[toLoad][3];;
  KD =  objectControlSet[toLoad][4];;
  
}

//sets all of the control parameters based on the given setPoint and the valid control sets 
void setControlVals(float setPoint){
  //make sure is a set point that is in the table 
  if (setPoint > movementControlSets[0][0] || setPoint < movementControlSets[maxCtrlSetIndex][0]){
    debugPrint("invalid setPoint, not in control set range: " + String(setPoint)); 
    return;
  }
    
  int indexL =1;
  int indexH =0;  
  while (indexL < maxCtrlSetIndex){
    if ( setPoint <= movementControlSets[indexH][0] && setPoint >= movementControlSets[indexL][0]) break; 
    indexL ++;
    indexH ++;
  }
  
  float lowVal= movementControlSets[indexL][0];
  float highVal = movementControlSets[indexH][0];
  
  //update the system control parameters 
  currSetPoint =  setPoint;
  
  currPWMMidpointFieldStr = floatMap(setPoint,lowVal,highVal,movementControlSets[indexL][1],movementControlSets[indexH][1]);
  currPWMMidpoint = findBestCalibratePWM(currPWMMidpointFieldStr);
  
  KP =  floatMap(setPoint,lowVal,highVal,movementControlSets[indexL][2],movementControlSets[indexH][2]);
  KI =  floatMap(setPoint,lowVal,highVal,movementControlSets[indexL][3],movementControlSets[indexH][3]);
  KD =  floatMap(setPoint,lowVal,highVal,movementControlSets[indexL][4],movementControlSets[indexH][4]);

}

//maps value s in range a to a value in range b 
float floatMap(float s, float a1, float a2, float b1, float b2){
  return b1 + ( (s-a1)*(b2-b1) ) / (a2-a1);
}

//sets the duty of the PWM to the decimal given
void setCoilPWM(float dutyPercent) {

  //update PWM makeing  sure duty is valid
  currPWM = constrain(dutyPercent, 0, 1);
  
  //calculate counter value that will result in given duty, and update the duty in the macrocell. 
  currPWMCounterVal = MAX_PWM_COUNT * currPWM;
  pwm_write_duty( PIN_COIL, currPWMCounterVal);
}


//================================================================================
//                                 Setup and Interrupt Methods
//================================================================================

//sets up the interrupt for the PWM
void setupCoilPWM() {
  // Set PWM Resolution
  pwm_set_resolution(16);
  pwm_setup( PIN_COIL, PWM_FREQ, 1);

  currPWM = 0;

  //Duty Cycle
  setCoilPWM(currPWM);
}

//sets up the timer that triggers the ADC for the moveing mean
void setupMMTimer() {
  pmc_enable_periph_clk (TC_INTERFACE_ID + 0 * 3 + 0) ; // clock the TC0 channel 0

  TcChannel * t = &(TC0->TC_CHANNEL)[0] ;    // pointer to TC0 registers for its channel 0
  t->TC_CCR = TC_CCR_CLKDIS ;  // disable internal clocking while setup regs
  t->TC_IDR = 0xFFFFFFFF ;     // disable interrupts
  t->TC_SR ;                   // read int status reg to clear pending
  t->TC_CMR = TC_CMR_TCCLKS_TIMER_CLOCK1 |   // use TCLK1 (prescale by 2, = 42MHz)
              TC_CMR_WAVE |                  // waveform mode
              TC_CMR_WAVSEL_UP_RC |          // count-up PWM using RC as threshold
              TC_CMR_EEVT_XC0 |     // Set external events from XC0 (this setup TIOB as output)
              TC_CMR_ACPA_CLEAR | TC_CMR_ACPC_CLEAR |
              TC_CMR_BCPB_CLEAR | TC_CMR_BCPC_CLEAR ;

  t->TC_RC =  VARIANT_MCK / 2 / MM_FREQ; // counter resets on RC, so sets period in terms of 42MHz clock
  t->TC_RA =  (VARIANT_MCK / 2 / MM_FREQ) / 2 ; // roughly square wave
  t->TC_CMR = (t->TC_CMR & 0xFFF0FFFF) | TC_CMR_ACPA_CLEAR | TC_CMR_ACPC_SET ;  // set clear and set from RA and RC compares

  t->TC_CCR = TC_CCR_CLKEN | TC_CCR_SWTRG ;  // re-enable local clocking and switch to hardware trigger source.
}

void setupADC ()
{
  NVIC_EnableIRQ (ADC_IRQn) ;   // enable ADC interrupt vector
  ADC->ADC_IDR = 0xFFFFFFFF ;   // disable interrupts
  ADC->ADC_IER = 0x80 ;         // enable AD7 End-Of-Conv interrupt (Arduino pin A0)
  ADC->ADC_CHDR = 0xFFFF ;      // disable all channels
  ADC->ADC_CHER = 0x3CFF ;    // ch7:A0 ch6:A1 ch5:A2 ch4:A3 ch3:A4 ch2:A5 ch1:A6 ch0:A7 ch10:A8 ch11:A9 ch12:A10 ch13:A11
  ADC->ADC_CGR = 0x15555555 ;   // All gains set to x1
  ADC->ADC_COR = 0x00000000 ;   // All offsets off

  ADC->ADC_MR = (ADC->ADC_MR & 0xFFFFFFF0) | (1 << 1) | ADC_MR_TRGEN ;  // 1 = trig source TIO from TC0
}


//Moveing Mean read Hall Interrupt
//keeps a moving mean going in the MMSum value is retruenved by read hall method
//is called whenever the ADC7 (A0) is finished with its convertion
void ADC_Handler (void) {
  //get rid of old val
  MMSum -= MMVals[nextMMToRead];

  if (readFromCoilHall){
      //wait untill all 4 ADCs have finished thier converstion. 
      while(!(((ADC->ADC_ISR & ADC_ISR_EOC10) && (ADC->ADC_ISR & ADC_ISR_EOC11) && (ADC->ADC_ISR & ADC_ISR_EOC12) && (ADC->ADC_ISR & ADC_ISR_EOC13))));
      //add all of adc values up and bit shit right two times to devide by 4
      //typcast to unsigned int so leading bits are shifted in as 0s
      MMVals[nextMMToRead] = ((unsigned int)(*(ADC->ADC_CDR+10)+*(ADC->ADC_CDR+11)+*(ADC->ADC_CDR+12)+*(ADC->ADC_CDR+13))) >>2;
      //read ch7 value to clear the end of convertion bit 
      *(ADC->ADC_CDR+7);
  }
  else{
      //wait untill all 8 ADCs have finished thier converstion. 
      while(!(((ADC->ADC_ISR & ADC_ISR_EOC7) && (ADC->ADC_ISR & ADC_ISR_EOC6) && (ADC->ADC_ISR & ADC_ISR_EOC5) && (ADC->ADC_ISR & ADC_ISR_EOC4)
       && (ADC->ADC_ISR & ADC_ISR_EOC3) && (ADC->ADC_ISR & ADC_ISR_EOC2) && (ADC->ADC_ISR & ADC_ISR_EOC1) && (ADC->ADC_ISR & ADC_ISR_EOC0))));
      
      //add all of adc values up and bit shit right three times to devide by 8
      //typcast to unsigned int so leading bits are shifted in as 0s
      MMVals[nextMMToRead] = ((unsigned int)(*(ADC->ADC_CDR+7)+*(ADC->ADC_CDR+6)+*(ADC->ADC_CDR+5)+*(ADC->ADC_CDR+4)+*(ADC->ADC_CDR+3)+*(ADC->ADC_CDR+2)+
      *(ADC->ADC_CDR+1)+*(ADC->ADC_CDR+0))) >>3;
  }

  MMSum += MMVals[nextMMToRead];
  //update next MM tht should be read in
  nextMMToRead++;
  nextMMToRead = nextMMToRead % NUM_MM_VALS;

}

// Loop in this method until Arduino
// begins to talk to processing code
void setupConnection() {
  if (!REQUIRE_SERIAL || digitalRead(PIN_REQUIRE_SERIAL) == LOW) {
    return;
  }
  prevExecute = micros();
  while (Serial.available() <= 0) {
    Serial.println("H");
    digitalWrite(PIN_STATE_MODE_OFF, !digitalRead(PIN_STATE_MODE_OFF));
    digitalWrite(PIN_STATE_MODE_IDLE, !digitalRead(PIN_STATE_MODE_IDLE));
    if ( micros() > (prevExecute + HANDSHAKE_WAIT_TIME)) return;
    delay(200);
  }
  //acknowledgement to take it out of arduino buffer 
  Serial.read();
  
  pHandShake = true;
  Serial.println("~"+ String(maxMinSetPoint[0])+ "," + String(maxMinSetPoint[1]));

}

//initializes values used for control set assumes there is at least one control set value
void setupControlSet(){
  maxCtrlSetIndex = sizeof(movementControlSets)/ sizeof(movementControlSets[0])-1;
  maxMinSetPoint[0]= movementControlSets[0][0];
  maxMinSetPoint[1]= movementControlSets[maxCtrlSetIndex][0];
}

void setup() {
  Serial.begin(115200);
  //init system state LED pins and set thier values to indicate system is waiting for connection
  pinMode(PIN_STATE_MODE_OFF, OUTPUT);
  pinMode(PIN_STATE_MODE_IDLE, OUTPUT);
  pinMode(PIN_STATE_MODE_PID_CONTROL, OUTPUT);
  pinMode(PIN_ENABLE, INPUT);
  pinMode(PIN_REQUIRE_SERIAL, INPUT);
  digitalWrite(PIN_STATE_MODE_OFF, HIGH);
  digitalWrite(PIN_STATE_MODE_IDLE, HIGH);
  digitalWrite(PIN_STATE_MODE_PID_CONTROL, LOW);
  
  setupControlSet();
  
  // Don't continue until connection to processing code is established
  setupConnection();

  statusPrint("SETUP");

  lastPrint = micros();
  prevMove = micros();
  printCounter = 0; 
  autoPrint = false;

  
  //setup system interrupts
  setupCoilPWM();
  setupADC ();
  setupMMTimer();

  // set the PWM to zero and wait a bit to make sure Moveing mean array is populated
  setCoilPWM(0);
  delay(1);
  setMode(MODE_CALIBRATE);

  //current PID control wieghts  (will be adjusted for tuning the PID controller)
  KP = INIT_KP;
  KI = INIT_KI;
  KD = INIT_KD;
  currPWMMidpointFieldStr = INIT_PWM_MIDPOINT_FIELD_STR;
  currSetPoint = INIT_SET_POINT;
}

void loop() {
  // User commands.
  if ( Serial.available() ) {
    processUserInput();
  }

  //auto print if set
  conditionalPrint();
  
  //make sure is enabled if not turn system off and return 
  if (digitalRead(PIN_ENABLE) == LOW){
    enabled = false;
    setCoilPWM(0);
    return; 
  } 
  //enable system if it was disabled and the enable pin is high 
  else if (!enabled){
    enabled = true;
  }
  
  executeMove();
    
  //execute proper mode
  switch ( currMode )
  {
    case MODE_OFF:
      executeModeOff();
      break;
    case MODE_CALIBRATE:
      executeModeCalibrate();
      break;
    case MODE_IDLE:
      executeModeIdle();
      break;
    case MODE_PID_CONTROL:
      executeModePIDControl();
      break;
    default:
      break;
  }
}


