// File: MagneticManipulatorGUI.pde
// Authors:
//    John Olennikov
//    Marley Rutkowski
// Notes:
//  --> BBCC_Plotter used for graph plotting (although it looks nothing like the original code)

/*
 * BBCC_Plotter
 * by Tim Hirzel, Feb 2008
 * v 1.1
 *
 * with gratitude, based on:
 * Grapher Pro!
 * by Tom Igoe
 *
 * This Processing application is designed to plot and display values 
 * coming from the BBCC PID controller for Arduino.  With minimal modification,
 * it could be setup to plot and show different incoming data
 *
 * Please refer to the top section for alterations of plot ranges, graph size etc.
 *
 * All code released under
 * Creative Commons Attribution-Noncommercial-Share Alike 3.0 
 */

import processing.serial.*;

// Constants
// TODO: Should a ScreenProperties object be create
// to hold this information or are global constants fine?
// Are constants faster than an OOP object?
int SCREEN_HEIGHT = 675;
int SCREEN_WIDTH = 1250;
int SCREEN_PADDING_TOP = 15;
int SCREEN_PADDING_RIGHT = 15;
int SCREEN_PADDING_BOTTOM = 15;
int SCREEN_PADDING_LEFT = 15;

// Program Background Color
int BACKGROUND_COLOR = 125;
int baseColor = 0;

// Turn off during production
boolean DEBUG_LOG = true;
boolean SERIAL_ON = true;

// Plotter Objects
PlotterObject plotHall;     // Plots Hall Sensor value (current and expected)
PlotterObject plotPID;      // Plots PID constants
PlotterObject plotPWM;      // Plots the PWM signal

// Console Object
ConsoleObject cout;

// Number Selector Object
SelectNumber setHallVal;

// Serial port object and related vars
Serial sPort;
String sVal;
boolean sHandshake;

/*
 * setup()
 *
 * First method that gets called when Processing starts up.
 *
 * Setup the canvas and the GUI components. 
 * All GUI objects are setup with respect to "SCREEN_*" constants.
 */
void setup() {
  // Setup canvas
  size(SCREEN_WIDTH,SCREEN_HEIGHT);
  baseColor = color(BACKGROUND_COLOR);
  
  // Plots the Hall sensor values
  // Set x and y position with respect to SCREEN_ config
  plotHall = new PlotterObject(SCREEN_PADDING_LEFT+50, SCREEN_PADDING_TOP, "Hall Sensor Values");
  // Accept and Plot "SetPoint" and "HALL" values
  plotHall.setupSensors(new String[] {"SetPoint","HALL"}, new boolean[] {true,true});
  // Setup x & y axis, min and max values, and grid space { min, max, grid }
  plotHall.setupAxels(new int[] { 0, 200, 40}, new int[] {-10, 110, 10});
  // Setup x & y axis labels
  plotHall.setLabels("Seconds","Percent of Range");
  
  // Adjusts Hall Sensor value for Hall sensor plotter
  // Set x and y position with respect to HALL plotter
  setHallVal = new SelectNumber(plotHall.getPlotXPos() + plotHall.getPlotWidth()/4, plotHall.getYPosOffset() + 75);
  
  // Plots the PID constants
  // Set x and y position with respect to HALL plotter and SCREEN_ config
  plotPID = new PlotterObject(plotHall.getXPosOffset() + 150, SCREEN_PADDING_TOP, "PID Tuning");
  // Accept and Plot "P", "I" and "D" values
  plotPID.setupSensors(new String[] {"P", "I", "D"}, new boolean[] {true, true, true});
  // Setup width and height
  plotPID.setDimension(450, 200);
  // Setup x & y axis, min and max values, and grid space { min, max, grid }
  plotPID.setupAxels(new int[] { 0, 200, 40}, new int[] {-20, 20, 5});
  // Setup x & y axis labels
  plotPID.setLabels("Seconds","Rel Contrib");
  
  // Plots PWM values
  // Set x and y position with respect to PID plotter
  plotPWM = new PlotterObject(plotPID.getXPos(), plotPID.getYPosOffset() + 50, "Pulse Width Modulation");
  // Accept and Plot "PWM" value
  plotPWM.setupSensors(new String[] {"PWM"}, new boolean[] {true});
  // Setup width and height
  plotPWM.setDimension(450, 200);
  // Setup x & y axis, min and max values, and grid space { min, max, grid }
  plotPWM.setupAxels(new int[] { 0, 200, 40}, new int[] {0, 100, 10});
  // Setup x & y axis labels
  plotPWM.setLabels("Seconds","Duty %");
  
  // Console/TextOuput Object
  // Set x and y position with respect to SCREEN_ config
  cout = new ConsoleObject(SCREEN_PADDING_LEFT, SCREEN_HEIGHT-50);
  // Setup width and height values with respect to SCREEN_ config
  cout.setDimension(int(SCREEN_WIDTH*0.8), int(SCREEN_HEIGHT*0.8));
  // Adjust x and y position to set console in bottom center of screen
  cout.setPosition((SCREEN_WIDTH - cout.widthVal)/2, cout.label.getYPos());

}

/*
 * draw()
 *
 * Loop that is called by Processing for repaining the canvas
 *
 * Individually paint each GUI component
 */

void draw() {
  background(baseColor);
  
  // Generate random data points to plot on graphs for testing
  // NOTE: VERY Inefficient. Didn't take time to figure out why
  //   it slows down Processing almost to a halt
  //   Probably due to the fact that it's inside the "draw" loop
  //generateDataPoints(plotPID);
  //generateDataPoints(plotHall);
  
  // helps "moveTo" method determine how much longer to raise object
  checkMoveGoalReached();
  
  // Paint plotter graphs
  plotHall.paint();
  plotPID.paint();
  plotPWM.paint();
  
  // Point hall value selector
  setHallVal.paint();
  
  // Paint console object
  cout.paint();
}

//==============================================================================
//=                              Event Handlers                                =
//==============================================================================

// Mouse events handler
// mouseX and mouseY are "passed in" by Processing. Just use them.
void mousePressed() {
  // Check if console was clicked
  if (cout.containsPoint(mouseX, mouseY)) {
    cout.debugLog("Event[click]: counsoleOutputFocus");
    // Notify console that it has been clicked
    cout.setActive(true);
  
  // Check if Hall value selector was clicked
  } else if (setHallVal.containsPoint(mouseX, mouseY)) {
    cout.debugLog("Event[click]: setHallValFocus");
    // Handle onClick event
    setHallVal.onClick(mouseX, mouseY);
    // Notify Hall Plotter that it has been clicked
    setHallVal.setActive(true);
    
  // Check if Hall Plotter graph was clicked
  } else if (plotHall.containsPoint(mouseX, mouseY)) {
    cout.debugLog("Event[click]: plotHallFocus");
    
    // If the mouseclick was inside the "graph" part
    // Initiate moveTo method to raise magnet to that height
    if (mouseX >= plotHall.getPlotXPos() && mouseX <= plotHall.getPlotXPosOffset()) {
      // Calculate graph scalar values
      float diff = plotHall.getPlotYPosOffset() - plotHall.getPlotYPos();
      float one_twelfth = diff/12;
      float minVal = plotHall.getPlotYPos() + one_twelfth;
      float maxVal = plotHall.getPlotYPosOffset() - one_twelfth;
      
      // Determine value to move to
      float percent = (1-((mouseY - minVal)/(maxVal - minVal)));
      setHallVal.number = (int)(percent*100.0);
      cout.debugLog("Percent value: "+Integer.toString(setHallVal.number));
      // Update Hall value selector
      setHallVal.setValue(Integer.toString(setHallVal.number));
      // Call moveTo method to change magnet height
      moveTo(Integer.parseInt(setHallVal.getValue())/100.0);
    }
  
  // If the "background" has been clicked, make sure each GUI
  // component knows that they are no longer active
  } else {
    // Hall value selector
    if (setHallVal.getActive()) {
      setHallVal.setActive(false);
      cout.debugLog("Event[click]: setHallValBlur");
    }
    // Console object
    if (cout.getActive()) {
      cout.setActive(false);
      cout.debugLog("Event[click]: counsoleOutputBlur");
    }
  }
  
}

// Keyboard Event handler
// The pressed key and keycode values are passed in by Processing. Just use them.
// TODO: Figure out how to use a helper method
void keyPressed() {
  // If typing into the console
  // Note: console has priority over hall value selector
  if (cout.getActive()) {
    if (keyCode == BACKSPACE) {
      // Remove the last character
      if (cout.getValue().length() > 0) {
        cout.setValue(cout.getValue().substring(0, cout.getValue().length()-1 ));
      } else {
        cout.setValue("");
      }
    } else if (key != CODED) {
      if (key != '\n') {
        cout.setValue(cout.getValue() + key);
      } else if (trim(cout.getValue()).length() > 0) {
        parseCmd(trim(cout.getValue()));
        cout.setValue("");
      }
      
    }
    
  // If typing into the Hall value selector
  } else if (setHallVal.getActive()) {
    if (keyCode == BACKSPACE) {
      // Remove the last character
      if (setHallVal.getValue().length() > 0) {
        setHallVal.setValue(setHallVal.getValue().substring(0, setHallVal.getValue().length()-1 ));
      } else {
        setHallVal.setValue("");
      }
    } else if (key != CODED) {
      if (key != '\n') {
        setHallVal.setValue(setHallVal.getValue() + key);
      } else if (trim(setHallVal.getValue()).length() > 0) {
        moveTo(Float.parseFloat(setHallVal.getValue())/100.0);
      }
      
    }
  }
}

// Handles data being received from the Arduino via Serial
void serialEvent(Serial sPort) {
  // store incoming data as a newline-delimited (\n) string
  sVal = sPort.readStringUntil('\n');
  
  if (sVal != null) {
    // trim whitespace
    sVal = trim(sVal);
    
    // Ensure that both sides are in sync with each other
    if (sHandshake == false) {
      // Received the "handshake" value
      if (sVal.equals("H")) {
        // Clear serial queue
        sPort.clear();
        sHandshake = true;
        
        cout.debugLog("Writing ACK.");
        
        // Notify Arduino that handshake was received
        sPort.write("A");
        
        cout.debugLog("Status: Handshake Token Received!");
        cout.stdLog("Status: Connection Established.");
      
      } else {
        cout.debugLog("Error: Invalid Handshake Token! -> (\"" + sVal + "\")");
        cout.stdLog("Status: Failed to Establish Connection.");
        
      }
    } else {
      readFromArduino(sVal);
    }
  }
}

//=============================================================================
//                                  Serial Methods
//=============================================================================

void listSerialDevices() {
  cout.stdLog("Serial Devices:");
  for(int ix=0; ix<Serial.list().length; ++ix) {
    cout.stdLog("Device ["+ix+"]: "+Serial.list()[ix]); 
  }
  
}

// Connects to selected serial device
// Does bounds checking as well
void connectSerialDevice(int ix) {
  // If selected serial device is connected
  if(Serial.list().length - 1 >= ix && ix>0) {
    cout.debugLog("Connecting to serial device: \""+Serial.list()[ix]+"\"");
    // Close existing serial port (if already set up)
    if(sPort != null) {
      sPort.stop();
    } 
    
    // Initialize serial port and set baud rate to 115200
    // Note: higher baud rate means more data being sent in same amount of time
    sPort = new Serial(this, Serial.list()[ix], 115200);
    sPort.bufferUntil('\n');
    cout.stdLog("Serial Device Connected!");
 // If not serial devices are connected
 } else {
   cout.stdLog("Error: Please select a valid serial device.");
 }
}

// Closes serial connection to connected device
void closeSerialDevice() {
  // Close existing serial port (if already set up)
  if(sPort != null) {
    sPort.stop();
  }
  sHandshake = false; 
}

/*
 * List of Commands:
 *
 * -> serial
 *   --> help - display usage
 *   --> list - list attached serial devices
 *   --> open {number} - select a serial device 
 *   --> close - close current open connection 
 * -> plotter
 * -> debug
 * -> help
 */
// Update console and parse command
void parseCmd(String str) {
  // Write cmd to console
  cout.stdLog(">$: "+str);
  String[] cmd = str.split(" ");
  
  // Debug
  cout.printStringArray(cmd);
  
  // Parse "serial" command
  if (cmd[0].equals("serial")) {
    if (cmd.length > 1) {
      // --> serial list
      if (cmd[1].equals("list")) {
        listSerialDevices();
      // --> serial open ##
      } else if (cmd[1].equals("open") && cmd.length >= 3) {
        connectSerialDevice(int(cmd[2]));
      // --> serial close
      } else if (cmd[1].equals("close")) {
        closeSerialDevice();
      } else {
        usageSerial();
      }
    } else {
      usageSerial();
    }
    
  // Parse "plotter" command
  } else if(cmd[0].equals("plotter")) {
    usagePlotter();
    
  // Parse "debug" command
  } else if (cmd[0].equals("debug")) {
    usageDebug();
    
  // Parse "help" command
  } else if (cmd[0].equals("help")) {
    printHelp();
    
  // Arduino handles rest of commands  
  } else {
    //TODO temporary. once commands are normal, use single method
    sendToArduino(str);
  }
}

//=============================================================================
//                                Console Feedback
//=============================================================================

void printHelp() {
  cout.stdLog(
    "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"+
    "Welcome to Magnetic Manipulator\n"+
    "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"+
    "Commands:\n"+
    "    debug\n"+
    "    help\n"+
    "    plotter\n"+
    "    serial\n"
  );
}

void usageDebug() {
  cout.stdLog(
    "debug - Manage Debugging Options\n"+
    "Usage: debug <command> \n"+
    "\n NOT YET IMPLEMENTED!\n"
  );
}

void usageSerial() {
  cout.stdLog(
    "serial - Manage Serial Devices\n"+
    "Usage: serial <command>\n"+
    "<commands>\n"+
    "    help            Display this help menu.\n"+
    "    list            List all serial devices.\n"+
    "    open <num>      Open connection to serial devices.\n"+
    "    close           Close connection to active device.\n"+
    "    handshake       Reconnect to Arduino if handshake is lost.\n"
  );
}

void usagePlotter() {
  cout.stdLog(
    "plotter - Manage Graph Plotter\n"+
    "Usage: plotter <command> \n"+
    "\n NOT YET IMPLEMENTED!\n"
  );
}

//TODO convert single char user input to normal code
void sendToArduino(String mesg) {
  String[] cmd;
  
  // Don't continue unless connected to serial device
  if (!SERIAL_ON) {
    cout.stdLog("Error: Unknown Command.");
    return;
  }
  
  cout.debugLog("Command Received: [" + mesg + "]");

  if (sPort == null) {
    cout.stdLog("Error: No Serial Device Detected.");
  } else {
    cmd = mesg.split(" ");
    // Reduce p val
    if (cmd[0].equals("p")) {
      sPort.write("p");
    // Increase P val
    } else if (cmd[0].equals("P")) {
      sPort.write("P");
    // Reduce I val
    } else if (cmd[0].equals("i")) {
      sPort.write("i");
    // Increase I val
    } else if (cmd[0].equals("I")) {
      sPort.write("I");
    // Reduce D val
    } else if (cmd[0].equals("d")) {
      sPort.write("d");
    // Increase D val
    } else if (cmd[0].equals("D")) {
      sPort.write("D");
    // Reduce magnet setpoint val
    } else if (cmd[0].equals("s")) {
      sPort.write("s");
    // Increase magnetic setpoint val
    } else if (cmd[0].equals("S")) {
      sPort.write("S");
    // Reduce PWM midpoint val
    } else if (cmd[0].equals("m")) {
      sPort.write("m");
    // Increase PWM midpoint val
    } else if (cmd[0].equals("M")) {
      sPort.write("M");
    // Lower magnet
    } else if (cmd[0].equals("u")) {
      sPort.write("u");
    // Raise magnet
    } else if (cmd[0].equals("U")) {
      sPort.write("U");
    // Print debug info
    } else if (cmd[0].equals("o")) {
      sPort.write("o");
    // Toggle auto print debug info
    } else if (cmd[0].equals("O")) {
      sPort.write("O");
    // Calibrate system
    } else if (cmd[0].equals("c")) {
      sPort.write("c");
    // Preset value for default object
    } else if (cmd[0].equals("0")) {
      sPort.write("0");
    // Preset value for object 1
    } else if (cmd[0].equals("1")) {
      sPort.write("1");
    // Preset value for object 2
    } else if (cmd[0].equals("2")) {
      sPort.write("2");
    // Preset value for object 3
    } else if (cmd[0].equals("3")) {
      sPort.write("3");
    // Set a continuous magnet lowering
    } else if (cmd[0].equals("-")) {
      setMoveDirDown();
    // Set a continuous magnet raising
    } else if (cmd[0].equals("+")) {
      setMoveDirUp();
    // Start or stop continuous magnet movement
    } else if (cmd[0].equals("=")) {
      toggleMove();
    // Set up and start continuous magnet movement
    } else if (cmd[0].equals("+=")) {
      moveUp();
    // Set down and start continuous magnet movement
    } else if (cmd[0].equals("-=")) {
      moveDown();
    } else {
      cout.stdLog("Error: Unknown Command.");
    }    
  } 
}

// Parse values read from the Arduino
// Each message is appended with "mesg type" token
void readFromArduino(String mesg) {
  // Extract token
  char ix = mesg.charAt(0);

  // DEBUG message type
  if (ix == '$') {
    cout.debugLog(mesg.substring(1, mesg.length()));
  // PLOTTER message type
  } else if (ix == '@') {
    //split appart the data meant fro each graph 
    String stream[] = split(mesg.substring(1, mesg.length()), '|');
    //update current setpoint
    String currSetPointString= split(stream[0], ',')[0];
    currSetPoint = Float.parseFloat(currSetPointString);
    //convert hall values to percents 
    String hallGraphPercentStr = ""; 
    for (String thisValStr : split(stream[0], ',')){
      String convertedValStr = convertHallToPercent(thisValStr);
      if (hallGraphPercentStr.length() == 0) hallGraphPercentStr = convertedValStr;
      else hallGraphPercentStr = hallGraphPercentStr + "," + convertedValStr;
    }    
    //send data point to hall plot
    parseData(hallGraphPercentStr, plotHall);
    //send data point to PWM plot 
    parseData(downConvertStream(stream[1],100), plotPWM);
    //send data point to PID plot 
    parseData(downConvertStream(stream[2],100), plotPID);

  // CONSOLE message type
  } else if (ix == '#') {
    cout.stdLog(mesg.substring(1, mesg.length()));
  
  // INIT message type
  } else if (ix == '~') {
    loadMaxMinStr(mesg);
  
  // UPDATE message type 
  } else if (ix == '!') {
    //arduino reached max and terminated move, notify gui accordingly
    currentlyMoving = false; 
    movingTo = false; 
    cout.stdLog(mesg.substring(1, mesg.length()));
    
  // UKNOWN - just print to console
  } else {
    cout.stdLog("<="+mesg+"=>");
  }
}

//================================================================================
//                                Movement Methods 
//================================================================================
//the max setpoint the Arduino can move to (index 0) and the min value (index 1)
//variable loaded on handshake with Arduino 
float maxMinSetPoint[] = {0,0};
float maxMinDiff; 
float currSetPoint = 0;  
float goalSetPoint = 0;
boolean currentlyMoving = false;
boolean movingTo = false;
boolean movingUp; 

String downConvertStream(String stream, int scale){
  String hallGraphPercentStr = "";
  for (String thisValStr : split(stream, ',')){
      float thisVal = Float.parseFloat(thisValStr);
      thisVal = thisVal / scale;
      String convertedValStr = Float.toString(thisVal);
      if (hallGraphPercentStr.length() == 0) hallGraphPercentStr = convertedValStr;
      else hallGraphPercentStr = hallGraphPercentStr + "," + convertedValStr;
  }   
  return hallGraphPercentStr;
}
String convertHallToPercent(String thisValStr){
      float thisVal = Float.parseFloat(thisValStr);
      thisVal = 100*(thisVal-maxMinSetPoint[1])/maxMinDiff;
      return Float.toString(thisVal); 
}

//checks whether goal has been reached for movingTo method 
void checkMoveGoalReached(){
  //if curently moving check whether goal has been reached
  if (currentlyMoving && movingTo){
    //stop moving if goal has been reached 
    if ((movingUp && currSetPoint >= goalSetPoint) || (!movingUp && currSetPoint <= goalSetPoint)){
      cout.stdLog("destination reached");
      movingTo = false;
      stopMove();
    }
  }
} 

//loads a string with comma deliminated max min values and a '~' designator 
void loadMaxMinStr(String mesg){
    String stream[] = split(mesg.substring(1, mesg.length()), ',');
    maxMinSetPoint[0] = Float.parseFloat(stream[0]);
    maxMinSetPoint[1] = Float.parseFloat(stream[1]);
    cout.stdLog("Load max = " + Float.toString(maxMinSetPoint[0]) + " max = "+ Float.toString(maxMinSetPoint[1]));
    maxMinDiff = maxMinSetPoint[0] - maxMinSetPoint[1]; 
}

void moveUp(){
  setMoveDirUp();
  initMove();
  cout.debugLog("move Up");
}

void moveDown(){
  setMoveDirDown();
  initMove();
  cout.debugLog("move Down");
}

void setMoveDirUp(){
  //changeing move dir terminates moveTo method 
  movingTo = false;
  movingUp = true;
  sPort.write("+");
}

void setMoveDirDown(){
  //changeing move dir terminates moveTo method 
  movingTo = false;
  movingUp = false;
  sPort.write("-");
}

void stopMove(){
  if (currentlyMoving) sPort.write("=");
  currentlyMoving = false; 
}

void initMove(){
  if (!currentlyMoving) sPort.write("=");
  currentlyMoving = true; 
}

void toggleMove(){
  sPort.write("=");
  currentlyMoving = !currentlyMoving; 
}

//initiates moveTo method towards position at given percent of available range
void moveTo(float percent){
  if (sPort == null) {
    cout.debugLog("Error: No serial device detected.");
    return;
  }
  //goal is relative to minimum setPoint
  goalSetPoint = maxMinSetPoint[1] + percent*maxMinDiff; 
  cout.stdLog("move to " + Float.toString(goalSetPoint) + " now at " + Float.toString(currSetPoint));
  //initate move toward goal 
  if(goalSetPoint > currSetPoint) {
    moveUp();
    movingTo = true;
  }
  else if (goalSetPoint < currSetPoint){
    moveDown();
    movingTo = true;
  }
  else {
     cout.debugLog("No move necessary already at goal destination");
  } 
}



//================================================================================
//                                Class Definitions
//================================================================================
// Holds the blueprint for all elements for
// object layout with respect to screen layout
class Element {
  int xPos, yPos;
  int widthVal = 400;
  int heightVal = 25;
  int baseColor = 0;
  int focusColor = 25;
  int baseAlpha = 255;
  
  // Debugging
  int timerStart;
  int timerStop;
  int timerHold;
  int timerDelay = 5;
  boolean timerRun;
  
  // TOP, RIGHT, BOTTOM, LEFT
  int[] padding = {0, 0, 0, 0};
  
  Element(int x, int y) {
    this.xPos = x;
    this.yPos = y;
  }
  
  // Returns actual width and height
  int getWidth() { return this.widthVal; }
  int getHeight() { return this.heightVal; }
  
  // Return actual X and Y positions and offsets
  int getXPos() { return xPos; }                       // LEFT
  int getYPos() { return yPos; }                       // TOP
  int getXPosOffset() { return xPos + widthVal; }      // RIGHT
  int getYPosOffset() { return yPos + heightVal; }     //BOTTOM
  
  // Returns width and height adjusted for padding
  int calcWidth() { return this.widthVal - padding[0] - padding[2]; }
  int calcHeight() { return this.heightVal - padding[1] - padding[3]; }
  
  // Returns X and Y positions and offsets adjusted for padding
  int calcXPos() { return xPos + padding[1]; }                    // LEFT
  int calcYPos() { return yPos + padding[0]; }                    // TOP
  int calcXPosOffset() { return getXPosOffset() - padding[3]; }   // RIGHT
  int calcYPosOffset() { return getYPosOffset() - padding[2]; }   // BOTTOM
  
  void setPosition(int x, int y) {
    xPos = x;
    yPos = y;
  }
  
  // Padding is stored as array, it is easier to create a helper method
  void setPadding(int t, int r, int b, int l) {
    padding = new int[] {t, r, b, l};
  }
  
  void setDimension(int w, int h) {
    widthVal = w;
    heightVal = h;
  }

  void setColors(int base, int focus) {
    baseColor = base;
    focusColor = focus;
  }
  
  // Check if x,y coord is contained in object
  boolean containsPoint(int x, int y) {
    return (x > calcXPos() && x < calcXPosOffset() && 
      y > calcYPos() && y < calcYPosOffset());
  }
  
  // Timer Debugging Code
  void initTimer() {
    timerStop = 0;
    timerHold = 0;
    timerRun = false;
    timerStart = 0;
  }
  
  void startTimer() {
    if (timerRun) {
      timerHold = timerStart;
    }
    timerRun = true;
    timerStart = millis();
  }
  
  void stopTimer() {
    timerStop = millis();
    timerRun = false;
    
    if (timerDelay < 10) {
      timerDelay++;
    } else {
      timerDelay = 0;
  
      if(timerHold != 0) {
        //cout.stdLog("First: " + (timerStart - timerHold) + ". Second: " + (timerStop - timerStart));
      } else {
        //cout.stdLog("First: " + (timerStop - timerStart));
      }
    }
  }
}

// Text Container GUI Object
class TextContain extends Element {
  boolean isActive = false;
  int txtSize = 12;
  String txt;
  
  // LEFT, TOP
  int[] textIndent = {5, 0};
  
  TextContain(int x, int y, String txt) {
    super(x, y);
    
    this.txt = txt;
  }
  
  // Text getters and setters
  String getText()  { return this.txt; }
  void setText(String txt)  { this.txt = txt; }  
  
  // User "click" getters and setters
  boolean getActive() { return isActive; }
  void setActive(boolean active) { isActive = active; }
  
  void paint() {
    paintContainer();
    paintText();
  }
  
  // Paint text background
  void paintContainer() {
    // Paint text container
    fill(baseColor, baseAlpha);
    stroke(255);
    rect(calcXPos(), calcYPos(), calcWidth(), calcHeight());
  }
  
  // Paint text string
  void paintText() {
    // Paint text
    fill(color(255));
    stroke(0);
    textSize(txtSize);
    textAlign(LEFT, CENTER);
    
    // Since text is aligned "CENTER", place text in center of text container
    text(txt, calcXPos() + textIndent[0], calcYPos() + calcHeight()/2 + textIndent[1]);
    resetText();
  }
  
  // Restore text alignment to default (left, bottom)
  void resetText() {
    textAlign(LEFT, BOTTOM);
    textSize(12);
  }
}

// Text container that allows user input
class InputContain extends TextContain {
  int delay = 10;
  int delayIx = 0;
  String title = "";
  String value = "";
  String tail = "";
  
  InputContain(int x, int y, String t) {
    super(x, y, "");
    
    title = t;
  }

  void paint() {
    updateText();
    paintContainer();
    paintText();
  }

  // Current value
  void setValue(String val) { value = val; }
  String getValue() { return value; }
  
  //TODO fix printing delay (I think it's the line with 2*delay)
  // Updates text and shows cursor (blinking)
  void updateText() {
    if (getActive()) {
      delayIx += 1;
    }
    
    // Blink the cursor
    if (delayIx > delay) {
      setText(title + value + "|" + tail);
      if (delayIx > 2*delay) {
        delayIx = 0;
      }
    } else {
      setText(title + value + " " + tail);
    }
  }
}

// Button GUI Object
class ButtonContain extends TextContain {
  TextContain label;
  
  ButtonContain(int x, int y, String text) {
    super(x, y, text);
    
    // base, focus
    setColors(150, 175);
    
    setDimension(200, 100);
    
    label = new TextContain(getXPos() + widthVal/2, getYPos() + heightVal/2, text);
    label.setColors(100, 125);
  }
}

// Console GUI Object
// Save a history of commands but only dispays range
//TODO: enable history scrolling and text copy
class ConsoleObject extends TextContain {
  InputContain label;
  
  // Store console history
  ArrayList<String> outputList = new ArrayList();
  int outputTos = 0;
  int outputMax = 150;
  int outputRange = 27;
  
  ConsoleObject(int x, int y) {
    super(x, y, "");
    
    this.baseColor = 50;
    this.focusColor = 75;
    this.baseAlpha = 175;
    
    this.label = new InputContain(x, y, "Console: ");
    this.label.setDimension(this.widthVal, this.label.heightVal);
    
    this.setPosition(x, y - this.heightVal);
    
    // Set width and height values
    setDimension(750, 600);
    
    label.setText("");
  }

  // Adjust both console input box and console text box width and height vals
  void setDimension(int w, int h) {
    // Update console text width and height
    this.widthVal = w;
    this.heightVal = h;
    
    // Update label width
    this.label.widthVal = w;
    
    // Update console getYPos() (since height has been changed)
    this.yPos = this.label.getYPos() - this.label.padding[3] - this.heightVal;
    
    this.outputRange = int(this.heightVal/outputRange); 
  }
  
  void setPosition(int x, int y) {
    this.xPos = x;
    this.yPos = y - heightVal;
    
    this.label.setPosition(x, y); 
  }
  
  // Paints input field, but only paints consol text if user
  // has clicked on input field (active)
  void paint() {
    label.paint();

    // If console is selected
    if (getActive()) {
      
      // Update console screen text
      this.setText(getOutput());
      
      paintContainer();
      paintText();
    }
  }
  
  // Since input field acts like label, update input
  // field values instead of console text values
  void setValue(String val) { label.setValue(val); }
  String getValue() { return label.getValue(); }
  
  void setActive(boolean active) {
    isActive = active;
    label.setActive(active);
  }
  
  boolean getActive() {
    return label.getActive();
  }
  
  // Check if x,y coord is contained in input field part
  boolean containsPoint(int x, int y) {
    // Check only label if not in focus
    return (label.containsPoint(x,y) || (
        x > getXPos() && x < getXPosOffset() && 
        y > getYPos() && y < getYPosOffset() ) && getActive() );
  }
  
  // Push string to log history queue
  // Account for strings with embedded newlines
  void stdLog(String str) {
    String[] mesg = str.split("\n");
    for(int ix=0; ix<mesg.length; ++ix) {
      addToLog(mesg[ix]);
    }
  }
  
  // If in debug mode, prepend debug status and
  // add to log history queue
  void debugLog(String str) {
    if (DEBUG_LOG) {
      String[] mesg = str.split("\n");
      for(int ix=0; ix<mesg.length; ++ix) {
        println("DBG: "+mesg[ix]);
      }
    }
  }
  
  // Cleanly handles user input and manages history
  void addToLog(String log) {
    cout.debugLog("adding \""+log+"\"");
    // Number of elements in list is smaller than maximum
    // allowed entries (size of console history)
    if(outputList.size() < outputMax) {
      outputList.add(log);
      
    // Maximum number of entried acheived
    // Loop around and start overwriting oldest entries
    } else {
      outputList.set(outputTos,log);
      outputTos = (outputTos+1) % outputMax;
    }
  }
  
  void printStringArray(String[] str) {
    cout.debugLog("Printing string array:");
    for(int ix=0; ix<str.length; ++ix) {
      cout.debugLog("["+ix+"]>"+str[ix]);
    }
  }
  
  // Generates a string from the outputList ArrayList
  // that contains history of log (pre-formatted for textbox)
  String getOutput() {
    String out = "";
    
    int jx = 0;
    // Determine start index for view window
    int kx = outputList.size() - outputRange;
    
    for (int ix=0; ix<outputRange; ++ix) {
      // Use ix + kx to handle negative values
      jx = ix + kx;
      if (jx >= 0) {
        out = out + '\n' + outputList.get(calculateIndex(jx));
      } else {
        out = out + '\n';
      }
    }
    
    return out;
  }
  
  // Calculate correct index when tos has moved from
  // the "0" position (overwriting history)
  int calculateIndex(int ix) {
    return (ix + outputTos) % outputMax;
  }
}

// Number Selector GUI Object
class SelectNumber extends Element {
  ButtonContain decVal;
  ButtonContain incVal;
  InputContain inputVal;
  
  int number = 0;
  int changeAmt = 10;
  
  int maxAmt = 100;
  int minAmt = 0;
  
  SelectNumber(int x, int y) {
    super(x,y);
    decVal = new ButtonContain(x, y, "<");
    decVal.setDimension(50, 25);
    
    inputVal = new InputContain(decVal.getXPosOffset(), y, "");
    inputVal.tail = "%";
    inputVal.setDimension(100, 25);
    inputVal.textIndent = new int[] {35, 0};
    inputVal.setValue("0");
    
    incVal = new ButtonContain(inputVal.getXPosOffset(), y, ">");
    incVal.setDimension(50, 25);
  }
  
  //TODO: This method is specifically for the Hall Plotter
  // Fix to be a generic method
  void onClick(int x, int y) {
    if (incVal.containsPoint(x, y)) {
      number += changeAmt;
      // Ensure not increased past max
      if (number > maxAmt) {
        number = maxAmt;
      }
      inputVal.setValue(Integer.toString(number));
      //TODO: don't embed in class?
      moveTo(number/100.0);
    } else if (decVal.containsPoint(x, y)) {
      number -= changeAmt;
      // Ensure not decreased past min
      if (number < minAmt) {
        number = minAmt;
      }
      inputVal.setValue(Integer.toString(number));
      //TODO: don't embed in class?
      moveTo(number/100.0);
    } else if (inputVal.containsPoint(x,y)) {
      inputVal.setActive(true);
    }
  }
  
  void paint() {
    decVal.paint();
    inputVal.paint();
    incVal.paint();
  }
  
  void setActive(boolean active) { inputVal.setActive(active); }
  boolean getActive() { return inputVal.getActive(); }
  
  void setValue(String val) {
    // Remove all non-numeric values
    val = val.replaceAll("[^0-9]","");
    
    if (val.length() <= 0) {
      inputVal.setValue("");
      return;
    }
    
    int number = Integer.parseInt(val);
    
    // Ensure chose number stays in bounds
    if (number > maxAmt) { number = maxAmt; }
    if (number < minAmt) { number = minAmt; }
    
    inputVal.setValue(Integer.toString(number));
  }
  
  String getValue() { return inputVal.getValue(); }
  
  boolean containsPoint(int x, int y) {
    return (x > decVal.calcXPos() && x < incVal.calcXPosOffset() && 
      y > decVal.calcYPos() && y < decVal.calcYPosOffset());
  }
}


//======================================================================
//                    Based off of BBCC_Plotter
//======================================================================

class PlotterObject extends Element {
  
  // milliseconds.  This just allows the axis labels in the X direction accurate
  int ExpectUpdateSpeed = 200;
  
  // These are all in real number space
  // all X values measured in ExpectedUpdateSpeed Intervals
  // all y measured in degrees
  int gridSpaceX = 25;  
  int gridSpaceY = 500; 
  int startX = 0;
  int endX = 200;
  int startY = 0;
  int endY = 5000;

  // calculated in constructor
  float pixPerRealY;
  float pixPerRealX;
  
  // Size of the background area around the plot
  int plotXBorder = 50;
  int plotYBorder = 50;
  
  // Graph labels and title
  String title;
  String yLabel;
  String xLabel;
  
  // The names of the values that get sent over serial
  String names[];
  // For each of the values, choose if you want it plotted or not here
  boolean[] plotName;

  int sensorCount;
  float[][] sensorValues;
  int currentValue;
  int hPosition;

  // Color Pallete
  int[][] colors;
  PFont titleFont;
  PFont labelFont;
  String fontType = "Courier";

  // Is supposed to be used to prevent uneccesary painting
  // Kinda useless in this context
  boolean updatePlot = false;
  
  int spaceVal = 15;
  boolean endReached = false;
  
  // Legend Object
  PlotterLegend legend;
  boolean displayLegend = true;

  PlotterObject(int x, int y, String title) {
    super(x, y);
    
    // the plot size in screen pixels
    setDimension(450, 400);
    
    this.title = title;
    setLabels("Seconds","Values");
    
    // Don't setup sensors. Assume user will update
    // When they create a new instance of object
    setupSensors(new String[] {}, new boolean[] {});
    
    // Position legend with respect to graph 
    legend = new PlotterLegend(getXPosOffset()-calcWidth()/10, getPlotYPos()+10, this);
    
    // Setup Color Pallete
    colors = new int[6][3];
    setupColors();
    
    smooth();
    titleFont = createFont(fontType, 18);
    labelFont = createFont(fontType, 14 );
    clearPlot();
  }
  
  // Getters and Setters for plot x & y values, dimensions, and offsets
  int getPlotWidth() { return calcWidth() - plotXBorder; }
  int getPlotHeight() { return calcHeight() - plotYBorder; }
  
  int getPlotXPos() { return calcXPos() + plotXBorder; }
  int getPlotYPos() { return calcYPos() + plotYBorder; }
  int getPlotXPosOffset() { return getPlotXPos() + getPlotWidth(); }
  int getPlotYPosOffset() { return getPlotYPos() + getPlotHeight(); }
  
  
  void setupSensors(String[] sensors, boolean[] printStat) {
    // Create array from string of sensor names
    names = sensors;
    plotName = printStat;
    
    // number of values to expect per x position on plot
    sensorCount = names.length;
    
    if(legend != null) {
      //TODO: change "22" to a calculated variable "line height"
      legend.setDimension(legend.getWidth(), sensorCount*22);
    }
    
    // array to hold the incoming values
    // endX-startX returns number of x data points on plotter
    // sensor count specifies which sensor's x value we are looking at
    sensorValues = new float[endX-startX][sensorCount];
    currentValue = 0;
    hPosition = startX;                                  // horizontal position on the plot
    
    // Setup Color Pallete
    //TODO maybe work with dynamic sensor count values...?
    //colors = new int[sensorCount][3];
  }
  
  // Paint the plotter
  void paint() {
    // Draw grid and labels
    clearPlot();
    
    // total number of x values
    int range = endX - startX;
    
    // draw the line of each sensor's data points
    for (int i = 0; i < sensorCount; i++) {
      // If not plotting sensor values, skip
      if (!plotName[i]) {
        continue;
      }
      
      // assign color to each plot
      stroke(colors[i][0], colors[i][1], colors[i][2]);
  
      // For each sensor's data point up to x position index "currentValue"
      // draw lines to connect each data point to it's previous data point
      for (int x = 1; x < range; x++) {
        line(realToScreenX(x-1), 
            realToScreenY(sensorValues[x-1][i]),
            realToScreenX(x),
            realToScreenY(sensorValues[x][i]));

        // Handles overwriting old datapoints instead of just clearing graph
        if (x >= range-1) {
          endReached = true;
        }
        // Generates a gap for separating old and new points
        if (x == currentValue) {
          x += spaceVal;
        }
        // Skip painting data points that have not been added (initial run)
        if (x >= currentValue && !endReached) {
          x = range;
        }
        
      }
    }
  
    if (hPosition >= endX) {
      hPosition = startX;
      // wipe the screen clean:
      clearPlot();
    } 
    else {
      hPosition += 1;  
    }
    
    noStroke();
    if (displayLegend) {
      legend.paint();
    }
  }
  
  // Draw grids and labels 
  void clearPlot() {
    //background(5);
    strokeWeight(1.5);
    stroke(10);
    fill(40);
    
    // draw boundary
    rect(getPlotXPos(), getPlotYPos(), getPlotWidth(), getPlotHeight());
    
    // draw title
    textAlign(CENTER);
    fill(70);
    textFont(titleFont);
    text(title, getPlotXPos() + getPlotWidth()/2, (getYPos() + getPlotYPos())/2);
  
    textFont(labelFont);
    stroke(10);
    
    //draw grid  
    fill(70);
    textAlign(RIGHT);
    for (int i = startY; i <= endY; i+= gridSpaceY) {
      line(getPlotXPos() - 3, realToScreenY(i), getPlotXPos() + getPlotWidth() - 1,  realToScreenY(i));
      text(str(i), getPlotXPos() - 10, realToScreenY(i));
    }
  
    textAlign(LEFT);
    for (int i = startX; i <= endX ; i+= gridSpaceX) {
      line(realToScreenX(i), getPlotYPos()+1, realToScreenX(i), getPlotYPos() + getPlotHeight() + 3);
      text(str((i)/ (1000 / ExpectUpdateSpeed)), realToScreenX(i), getPlotYPos() + getPlotHeight() + 20);
    }
  
    // Draw Axis Labels
    fill(70);
    // TODO: update to more general setting
    int vtxtSize = 20;
    // To center y axis text, find mid-point of plot and adjust mid-point of text to match that
    text(yLabel, getPlotXPos() - 70,  (getPlotYPos() + getPlotHeight()/2) - (vtxtSize*yLabel.length()/2)/2 );
  
    textAlign(CENTER);
    text(xLabel,  getPlotXPos() + getPlotWidth()/2, getPlotYPos() + getPlotHeight() + 50);
  }


  // Setup height and width values
  void setDimension(int w, int h) {
    widthVal = w;
    heightVal = h;
    
    setupPixelScale();
  }
  
  // Determines number of pixes per data point
  void setupPixelScale() {
    // divide the plot width by the number of data points to figure
    // out how number of pixels per data point
    pixPerRealX = float(getPlotWidth())/(endX - float(startX));
    pixPerRealY = float(getPlotHeight())/(endY - float(startY));
  }
  
  // Config = { xMin, xMax, xGrid }
  void setupAxels(int xConfig[], int[] yConfig) {
    // x coord info
    startX = xConfig[0];
    endX = xConfig[1];
    gridSpaceX = xConfig[2]; 
    
    // y coord info
    startY = yConfig[0];
    endY = yConfig[1];
    gridSpaceY = yConfig[2];
    
    // Adjust pixel scaling
    setupPixelScale();
  }
  
  // Setup labels
  void setLabels(String xVal, String yVal) {
    this.xLabel = xVal;
    
    // Adds newline between letters for vertical text display
    this.yLabel = "";
    for (int ix=0; ix<yVal.length(); ++ix) {
      this.yLabel += yVal.charAt(ix) + "\n";
    }
  }
  
  void setupColors() {
    // Thanks to colorbrewer for this pallete
    colors[0][0] = 102;  
    colors[0][1] =194; 
    colors[0][2] = 165;
    colors[1][0] = 252;  
    colors[1][1] = 141; 
    colors[1][2]= 98;
    colors[2][0] = 141;  
    colors[2][1] = 160; 
    colors[2][2]= 203;
    colors[3][0] = 231;  
    colors[3][1] = 138; 
    colors[3][2]= 195;
    colors[4][0] = 166;  
    colors[4][1] = 216; 
    colors[4][2]= 84;
    colors[5][0] = 255;  
    colors[5][1] = 217; 
    colors[5][2]= 47;
  }
  
  // Convert data to pixel position
  float realToScreenX(float x) {
    float shift = x - startX;
    return (getPlotXPos() + shift * pixPerRealX);
  }
  
  // Convert data to pixel position
  float realToScreenY(float y) {
    float shift = y - startY;
    return getPlotYPos() + getPlotHeight() - 1 - (shift) * pixPerRealY;
  }  
}

//======================================================================
//                    Based off of BBCC_Plotter
//======================================================================
class PlotterLegend extends Element {
  PlotterObject plotter;
  
  PlotterLegend(int x, int y, PlotterObject plotter) {
    super(x, y);
    
    // Keep reference of parent graph
    this.plotter = plotter;
    
    // the plot size in screen pixels
    this.widthVal = 130;
    
    // Each line height is about 21 pixels
    this.heightVal = plotter.sensorCount*21;
  }
  
  void setDimension(int w, int h) {
    this.widthVal = w;
    this.heightVal = h;
  }
  
  void paint() {
    fill(128,128,128,80);
    rect(xPos, yPos, widthVal, heightVal);

    // print the name of the channel being graphed:
    String line;
    for (int i = 0; i < plotter.sensorCount; i++) {
      fill(plotter.colors[i][0], plotter.colors[i][1], plotter.colors[i][2]);
      textAlign(LEFT);
      text(plotter.names[i] , getXPos()+5, getYPos() + (i+1) * 20);
      textAlign(RIGHT);
      text(nf(plotter.sensorValues[plotter.currentValue][i], 0,3), getXPos()+widthVal - 5, getYPos() + (i+1) * 20);
    }
  }  
}

//================================================================================
//                              BBCC_Plotter Code
//================================================================================

// This method is used only to test the plotter graphs
int tempIx = 0;
String DELIM = ",";
void generateDataPoints(PlotterObject plotter) {
  if (tempIx < 10) {
    tempIx += 1;
    return;
  }
  tempIx = 0;
  
  String serialString = "";
  
  for (int ix=0; ix<plotter.sensorCount; ++ix) {
    int r = int(random(plotter.startY,plotter.endY));
    
    serialString += str(r);
    if (ix + 1 != plotter.sensorCount) {
      serialString += DELIM;
    }
  }
  parseData(serialString, plotter);
}

// Extract data from delimited string and save to plotter
void parseData(String serialString, PlotterObject plotter) {
   // split it into substrings on the DELIM character:
  String[] numbers = split(serialString, DELIM);
  
  // make sure you're only reading as many numbers as
  // you can fit in the array
  if (numbers.length == plotter.sensorCount) {
    
    plotter.currentValue += 1;
    if (plotter.currentValue >= plotter.endX) {
      plotter.currentValue = 0;
    }
    
    // Note: bounds check happens above (no need here)
    for (int i = 0; i < numbers.length; i++) {
      // trim off any whitespace from the substring:
      numbers[i] = trim(numbers[i]);

      // Ensure data points don't get drawn outside graph boundaries
      if (float(numbers[i]) > plotter.endY) {
        numbers[i] = Integer.toString(plotter.endY);
      }
      if (float(numbers[i]) < plotter.startY) {
        numbers[i] = Integer.toString(plotter.startY);
      }
      
      // convert each substring into a float
      plotter.sensorValues[plotter.currentValue][i] = float(numbers[i]);
    }
    plotter.updatePlot = true;
    
  } else {
    cout.debugLog("Error: Number of values passed to plotter is invalid.");
  }
}
