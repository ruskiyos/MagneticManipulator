MagneticManipulator
===================

Arduino Project for Levitating a Magnet

The code that was loaded on the Arduino Due Microcontroller can be found in the MagneticManipulatorDue directory.

The Processing GUI code can be found in the MagneticManipulatorGUI directory. 

Code written for system testing can be found in the testing directory, The majority of these programs perform one small piece of functionality in isolation, however a particularly useful test program can be found in
testing/due/RunningAverageDUEALLPWM 	// this file allows a user to directly control the current field strength of the coil by sending % duty cycles over serial and view the field strength measured. To set field at 50% user would type “.5” to turn it off “0.0” or for full power “1.0”. All other duty cycles can be achieved by sending the appropriate decimal. 